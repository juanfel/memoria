from graph_connector import GraphConnector
from fuzzy import FuzzyFactory

connector = GraphConnector("localhost:7474", "neo4j", "Neo4j")

fuzzy_rules = FuzzyFactory.create_name_fuzzyrules()

fuzzy_rules.procesar_arbol_decision(connector, "NombresSimilares")

print(fuzzy_rules.similarity_ctrl.rules.all_rules)
