from py2neo import Graph
import glob


class GraphConnector(object):
    """Permite conectarse y dar consultas a la BDG"""

    def __init__(self, host, user, password):
        """TODO: to be defined1. """
        self.host = host
        self.user = user
        self.password = password

        self.graph = Graph(host, user=user, password=password)

    def query_de_prueba(self, nombre_re):
        """
        Envía el query de prueba a la base de dato y devuelve
        un iterable con los resultados.

        :param nombre_re: Expresión regular del nombre o apellido a buscar.
        """

        query = """
        MATCH (p:Persona)
        WITH p
        MATCH (p_duplicate:Persona) 
        WHERE  p <> p_duplicate
        AND p.sexo = p_duplicate.sexo
        AND (NOT (p:Bautizado) OR NOT (p_duplicate:Bautizado)) AND (NOT (p:Fallecido) OR NOT (p_duplicate:Fallecido))
        AND NOT (p)-[:Padre]-(p_duplicate)
        AND toLower(p.nombre + p.apellido_paterno + p.apellido_materno) =~ {nombre_fallecido}
        AND toLower(p_duplicate.nombre + p_duplicate.apellido_paterno + p_duplicate.apellido_materno) =~ {nombre_bautizado}
        return p, p_duplicate
        """
        resultados = self.graph.run(
            query, nombre_fallecido=nombre_re, nombre_bautizado=nombre_re)
        return resultados

    def get_similars(self, nombre_re=".*", threshold_nombre=0.65):
        """
        Obtiene todos los nodos de persona que tienen
        relaciones de similaridad
        """
        with open("cypher/get_similar_nodes.cypher") as similars:
            similar_nodes_query = similars.read()
        resultados = self.graph.run(similar_nodes_query, nombre_re=".*", threshold=threshold_nombre)
        return resultados

    def get_all_rels(self, threshold_nombre=0.65, threshold_tiempo=0.5):
        """
        Obtiene todos los nodos de personas con relaciones de diverso
        tipo, en base a su similirad.
        """
        with open("cypher/get_partitions.cypher") as similars:
            all_rels_query = similars.read()
        resultados = self.graph.run(all_rels_query, threshold_nombre=threshold_nombre, threshold_tiempo=threshold_tiempo)
        return resultados

    def get_nodes_in_partition(self, partition=0):
        """
        Obtiene todos los nodos que pertenecen a una partición
        """
        with open("cypher/get_nodes_in_partition.cypher") as file:
            query = file.read()
        resultados = self.graph.run(query, partition=partition)
        return resultados

    def do_clustering(self, threshold=0.5, algo="louvain"):
        """
        Hace los algorítmos para obtener clusters
        """
        query = ""
        if algo == "louvain":
            with open("cypher/louvain.cypher") as louvain:
                query = louvain.read()
        elif algo == "unionFind":
            with open("cypher/unionFind.cypher") as unionFind:
                query = unionFind.read()
        self.graph.run(query, threshold=threshold)

        ## Tambien propaga los clusters a los eventos de matrimonio
        with open("cypher/set_marriage_partitions.cypher") as file:
            query = file.read()
            self.graph.run(query)

    def do_graph_sim_queries(self):
        """
        Ejecuta queries que tengan que ver con la busqueda de 
        similaridades en base a la estructura del grafo
        """
        files = [
            "cypher/find_siblings.cypher",
            "cypher/find_sibling_similarities.cypher",
            "cypher/find_marriage_similarities.cypher",
            "cypher/find_parent_similarities.cypher",
            "cypher/find_fiance_son_similarities.cypher"
        ]
        queries = []

        for file in files:
            with open(file) as f:
                queries.append(f.read())
        for query in queries:
            self.graph.run(query)

    def get_graph_sim_scores(self):
        """
        Recupera los puntajes de las similitudes por
        grafo
        """
        with open("cypher/get_graph_similarity_scores.cypher") as f:
            query = f.read()
            resultados = self.graph.run(query)
        return resultados

    def delete_rels_with_no_graph_sims(self):
        """
        Elimina relaciones que no pudieron ser analizadas
        por similitud de grafo
        """
        with open("cypher/delete_rels_with_no_graph_sims.cypher") as f:
            query = f.read()
            resultados = self.graph.run(query)
        return resultados
    def get_distance_vector(self):
        """
        Devuelve el vector de distancias por particion
        """
        with open("cypher/get_distance_matrix_per_community.cypher") as vector:
            vector_query = vector.read()
        resultados = self.graph.run(vector_query)
        return resultados

    def get_rules(self, label):
        """
        Obtiene un árbol de reglas en base a su label.
        """
        with open("cypher/get_rules.cypher") as load_rules:
            rule_query = load_rules.read()

        resultados = self.graph.run(rule_query, NombreRegla=label)
        return resultados

    def get_rules_as_node_rels(self, label):
        """
        Obtiene el árbol de reglas con nodos y relaciones únicos
        """
        with open("cypher/get_rules_node_rels.cypher") as load_rules:
            rule_query = load_rules.read()

        resultados = self.graph.run(rule_query, NombreRegla=label)
        return resultados

    def filtrar_personas(self, nombre, a_paterno, a_materno, generaciones_mat, generaciones):
        """
        Ejecuta la consulta para filtrar las personas a mostrar en
        la visualización
        """
        self.unir_personas()
        with open("cypher/set_virtual_filter.cypher") as file:
            query = file.read()
            delete_labels_query = """
                //Elimina los filtros anteriores
                MATCH (a:Persona_Virtual)
                DETACH DELETE a
            """
            delete_marr_labels_query = """
            MATCH (b:Evento_Matrimonio_Virtual)
            DETACH DELETE b
            """

            delete_original_rels_query = """
            MATCH path=(n)-[r]-(p)
            WITH n,r,p
            WHERE (n:Persona_Virtual or n:Evento_Matrimonio_Virtual) AND (not (p:Persona_Virtual or p:Evento_Matrimonio_Virtual) and n <> p)
            OR ((p:Persona_Virtual or p:Evento_Matrimonio_Virtual) AND (type(r) = "Similitud" or type(r) = "Similitud_Apellidos"))
            DELETE r
            """
            self.graph.run(delete_labels_query)
            self.graph.run(delete_marr_labels_query)
            resultados = self.graph.run(query, nombre_re=nombre,
                                        apellido_paterno_re=a_paterno,
                                        apellido_materno_re=a_materno,
                                        generaciones_mat=generaciones_mat,
                                        generaciones=generaciones)
            self.graph.run(delete_original_rels_query)
            return resultados

    def camino_mas_corto(self,
                         nombre1,
                         a_paterno1,
                         a_materno1,
                         nombre2,
                         a_paterno2,
                         a_materno2):
        """
        Busca el camino mas corto entre las dos personas especificadas. Sus nombres
        y apellidos deben ir en  MINUSCULAS. Permite usar regex para buscar.
        """
        with open("cypher/shortest_paths.cypher") as file:
            query = file.read()
            resultados = self.graph.run(query,
                                        nombre_re=nombre1,
                                        apellido_paterno_re=a_paterno1,
                                        apellido_materno_re=a_materno1,
                                        nombre_re1=nombre2,
                                        apellido_paterno_re1=a_paterno2,
                                        apellido_materno_re1=a_materno2)
            return resultados

    def unir_personas(self):
        """
        Une todas las personas que tienen la misma partición
        """
        delete_person_query = """
        MATCH (p :Persona_Arbol)
        DETACH DELETE p
        """
        delete_marriage_query = """
        MATCH (p :Evento_Matrimonio_Arbol)
        DETACH DELETE p
        """
        delete_rels_query = """
        MATCH (n)-[r]-(p)
        WHERE ((n:Persona_Arbol or n:Evento_Matrimonio_Arbol ) and not (p:Persona_Arbol or p:Evento_Matrimonio_Arbol) and n <> p)
        AND (type(r) = "Similitud" or type(r) = "Similitud_Apellidos")
        DELETE r
        """
        self.graph.run(delete_person_query)
        self.graph.run(delete_marriage_query)
        with open("cypher/clone_nodes.cypher") as clone_file:
            query = clone_file.read()
            self.graph.run(query)

        with open("cypher/merge_virtual_nodes.cypher") as file:
            query = file.read()
            self.graph.run(query)

        with open("cypher/delete_rels_to_tree.cypher") as file:
            query = file.read()
            self.graph.run(query)

    def eliminar_similitudes(self):
        """
        Elimina relaciones de similitud del grafo
        """
        query = """
            MATCH (:Persona)-[r:Similar]-(:Persona)
            DELETE r
        """
        self.graph.run(query)

    def eliminar_db(self):
        """
        Más claro que el agua
        """
        delete_database = "MATCH (n) DETACH DELETE n"
        self.graph.run(delete_database)

    def cargar_arboles(self):
        """
        Cuando hay árboles hechos por cypher los carga
        a la DB
        """

        for arbol_file in glob.glob("cypher/arboles/*.cypher"):
            print("Cargando {0}".format(arbol_file))
            with open(arbol_file) as text:
                query = text.read()
                self.graph.run(query)


    def recargar_db(self):
        """
        Obtiene los datos de nuevo
        """
        with open('cypher/load_BAUT.cypher', 'r') as load_baut:
            load_baut_query = load_baut.read()

        with open('cypher/load_DEF.cypher', 'r') as load_def:
            load_def_query = load_def.read()

        with open('cypher/load_MAT.cypher', 'r') as load_mat:
            load_mat_query = load_mat.read()

        delete_database = "MATCH (n) DETACH DELETE n"

        with open('cypher/reglas_nombre.cypher') as create_rules:
            create_rules_query = create_rules.read()

        with open('cypher/reglas_tiempo.cypher') as create_time_rules:
            create_time_rules_query = create_time_rules.read()

        queries = [delete_database,
                   load_baut_query,
                   load_def_query,
                   load_mat_query
                   ]

        for query in queries:
            self.graph.run(query)

        self.cargar_arboles()

        reglas_nombres_params = {
            'regla_nombres_similares': {'label': 'NombresSimilares'},
            'var_apellido_paterno':  {'variable': 'ApellidoPaterno'},
            'var_apellido_materno':  {'variable': 'ApellidoMaterno'},
            'var_nombre':  {'variable': 'Nombres'},
            'res_nombre_mp_similar': {'consecuente': 'Iguales[mp_s]'},
            'res_nombre_p_similar':  {'consecuente': 'Iguales[p_s]'},
            'res_nombre_no_se':  {'consecuente': 'Iguales[ns]'},
            'res_nombre_p_no_sim':  {'consecuente': 'Iguales[p_ns]'},
            'res_nombre_mp_no_sim':  {'consecuente': 'Iguales[mp_ns]'},
            'res_parentezco_mp_similar': {'consecuente': 'Parientes[mp_s]'},
            'res_parentezco_p_similar':  {'consecuente': 'Parientes[p_s]'},
            'res_parentezco_no_se':  {'consecuente': 'Parientes[ns]'},
            'res_parentezco_p_no_sim':  {'consecuente': 'Parientes[p_ns]'},
            'res_parentezco_mp_no_sim':  {'consecuente': 'Parientes[mp_ns]'},
            'rel_mp_s': {'operacion': '&', 'funcion_membresia': 'mp_s'},
            'rel_p_s': {'operacion': '&', 'funcion_membresia': 'p_s'},
            'rel_ns': {'operacion': '&', 'funcion_membresia': 'ns'},
            'rel_p_ns': {'operacion': '&', 'funcion_membresia': 'p_ns'},
            'rel_mp_ns': {'operacion': '&', 'funcion_membresia': 'mp_ns'},
        }

        reglas_tiempo_params = {
            'regla_tiempos_coherentes': {'label': 'TiemposCoherentes'},
            'var_AVR': {'variable': 'AVR'},
            'var_FNI': {'variable': 'FNI'},
            'var_MDE': {'variable': 'MDE'},
            'var_EAN': {'variable': 'EAN'},
            'var_EAM': {'variable': 'EAM'},
            'var_EAB': {'variable': 'EAB'},
            'var_DEV': {'variable': 'DEV'},
            'res_coherencia_alta': {'consecuente': 'CoherenciaTemporal[Alta]'},
            'res_coherencia_baja': {'consecuente': 'CoherenciaTemporal[Baja]'},
        }
        self.graph.run(create_rules_query, **reglas_nombres_params)
        self.graph.run(create_time_rules_query, **reglas_tiempo_params)

    def start_tx(self):
        """
        Comienza una transacción
        """ 
        self.sec_graph = Graph(self.host, user=self.user, password=self.password)
        return self.sec_graph.begin()

    def push_to_graph(self, nodo):
        """
        Interfáz para subir un nodo o persona al gráfo. Usa transacciones
        """
        self.graph.push(nodo)

    def merge_to_graph(self, nodo):
        """
        Interfáz para crear un nodo nuevo. Usa transacciones.
        """
        self.tx.merge(nodo)

    def commit_tx(self):
        """
        Envía la transacción a la BD
        """
        self.tx.commit()
