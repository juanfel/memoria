CALL algo.louvain('Persona', 'Similitud', {weightProperty:'similitud_total',threshold:$threshold, defaultValue:0.0, write:true, writeProperty:"partition"})
YIELD nodes, communityCount, iterations, loadMillis, computeMillis, writeMillis
