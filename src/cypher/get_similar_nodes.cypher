MATCH (p:Persona)-[r:Similitud]->(:Persona)
WHERE r.similitud_nombre > $threshold
// WHERE p.Apellido_paterno = $nombre_re OR p.Apellido_materno = $nombre_re
RETURN DISTINCT p as persona
