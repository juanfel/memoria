match (p:Persona)-[r:Similitud]-(dup:Persona)
where exists(r.similitud_total)
WITH p, dup, r
MATCH (p)-[:Casado]-(:Matrimonio:Evento)-[:Casado]-(m1:Persona)
MATCH (dup)-[:Casado]-(:Matrimonio:Evento)-[:Casado]-(m2:Persona)
WITH p, m1, dup, m2, r
WHERE m1.sexo = m2.sexo AND m1 <> m2
OPTIONAL MATCH path=ShortestPath((m1)-[:Similitud*..6]-(m2))
WITH p, dup, path, m1, m2, r, CASE path WHEN null then 0 ELSE length(path) end as s,
	CASE path WHEN null then [{similitud_total:0}] ELSE relationships(path) end as rels
UNWIND rels as rel
with p, dup, path, m1,m2, s, avg(rel.similitud_total) as s_avg, r, rel
SET r.similitud_matrimonio = coalesce(s_avg, coalesce(1/s,0))
return p, dup, path, m1, m2, s, s_avg, r, rel
