match (a:Persona)-[:Padre]->(:Persona)<-[:Padre]-(b:Persona)
match path=(a)-[r_a:Similitud]-(a_p:Persona)-[:Casado]-(:Matrimonio)-[:Casado]-(b_p:Persona)-[r_b:Similitud]-(b)
WITH DISTINCT r_a, r_b
SET r_a.similitud_total = CASE 
							WHEN exists(r_a.similitud_total) THEN r_a.similitud_total + 0.1
                            ELSE 0.7
                         	END
SET r_b.similitud_total = CASE 
							WHEN exists(r_b.similitud_total) THEN r_b.similitud_total + 0.1
                            ELSE 0.7
                         	END
