MATCH 
    (padre1:Padre)-[:Padre]-(hermano1),
    (padre2:Padre)-[:Padre]-(hermano2)
WHERE padre1.nombre = padre2.nombre
MERGE (hermano1)-[:Hermano]-(hermano2)

MATCH 
    (madre1:Madre)-[:Madre]-(hermano1),
    (madre2:Madre)-[:Madre]-(hermano2)
WHERE madre1.nombre = madre2.nombre
MERGE (hermano1)-[:Hermano]-(hermano2)
