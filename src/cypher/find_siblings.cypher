match (p:Persona), (q:Persona)
where apoc.text.fuzzyMatch(p.apellido_paterno, q.apellido_paterno) 
	and apoc.text.fuzzyMatch(p.apellido_materno, q.apellido_materno)
    and p.apellido_materno <> "" and q.apellido_materno <> ""
    and id(p) <> id(q)
MERGE (p)-[:Similitud_Apellidos {parentezco:0.7}]-(q)
