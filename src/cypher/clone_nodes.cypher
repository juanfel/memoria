MATCH (n0:Persona)
WITH collect(n0) as n0_list
CALL apoc.refactor.cloneNodesWithRelationships(n0_list) yield input, output as n
call apoc.do.case(['Persona' in labels(n) , "set n:Persona_Arbol\n remove n:Persona", 'Evento_Matrimonio' in labels(n),
                            "set n:Evento_Matrimonio_Arbol\n remove n:Evento_Matrimonio"],"",{n:n}) yield value
return value
