LOAD CSV WITH HEADERS FROM "file:///all_DEF.csv" AS line 
WITH line,
     coalesce(line.Nombre, "") as Nombre,
     coalesce(line.Apellido_Materno, "") as A_Materno,
     coalesce(line.Apellido_Paterno, "") as A_Paterno,
     coalesce(line.Sexo, "") as Sexo,
     coalesce(line.Categoría, "") as Categoria,
     coalesce(line.Padre_Nombre, "") as Nombre_Padre,
     coalesce(line.Padre_Apellido_Paterno, "") as A_Paterno_Padre,
     coalesce(line.Padre_Apellido_Materno, "") as A_Materno_Padre,
     coalesce(line.Madre_Nombre, "") as Nombre_Madre,
     coalesce(line.Madre_Apellido_Paterno, "") as A_Paterno_Madre,
     coalesce(line.Madre_Apellido_Materno, "") as A_Materno_Madre,
     coalesce(line.Fecha, "") as Fecha,
     coalesce(line.Edad, "") as Edad,
     coalesce(line.Lugar, "") as Lugar,
     coalesce(line.`Estado Civil`, "") as Estado_Civil,
     coalesce(line.Pareja_Nombre, "") as Pareja_Nombre,
     coalesce(line.Pareja_Apellido_Materno, "") as Pareja_A_Materno,
     coalesce(line.Pareja_Apellido_Paterno, "") as Pareja_A_Paterno,
     coalesce(line.Pareja_Sexo, "") as Pareja_Sexo
CREATE 
//Crea los nodos
    (fallecido:Registro:Persona:Defuncion:Fallecido { 
                    nombre: Nombre,
                    apellido_paterno: A_Paterno,
                    apellido_materno: A_Materno,
                    lugar_entierro: Lugar,
                    fecha_entierro: Fecha,
                    edad_deceso: Edad,
                    estado_civil: Estado_Civil,
                    sexo: Sexo})

//Hay que asegurarse que los padres y pareja existan antes de crear información inutil
WITH Estado_Civil, Categoria, Pareja_Nombre, Pareja_A_Paterno, Pareja_A_Materno, Pareja_Sexo, Nombre_Padre, A_Paterno_Padre, A_Materno_Padre,
    Nombre_Madre, A_Paterno_Madre, A_Materno_Madre, fallecido
    CALL apoc.do.when(Nombre_Padre + A_Paterno_Padre + A_Materno_Padre <> "",
    "CREATE
        (padre:Padre:Registro:Persona:Defuncion {
                        nombre: Nombre_Padre,
                        apellido_paterno: A_Paterno_Padre,
                        apellido_materno: A_Materno_Padre,
                        sexo: 'Hombre'})
    MERGE (padre) -[:Padre]->(fallecido) RETURN padre","",
                            {fallecido:fallecido,
                            Nombre_Padre:Nombre_Padre,
                            A_Paterno_Padre:A_Paterno_Padre,
                            A_Materno_Padre:A_Materno_Padre}) yield value
WITH Estado_Civil, Categoria, Pareja_Nombre, Pareja_A_Paterno, Pareja_A_Materno, Pareja_Sexo, Nombre_Madre, A_Paterno_Madre, A_Materno_Madre,
    fallecido, value.padre as padre
    CALL apoc.do.when(Nombre_Madre + A_Paterno_Madre + A_Materno_Madre <> "",
    'CREATE
        (madre:Madre:Registro:Persona:Defuncion {
                    nombre: Nombre_Madre,
                    apellido_paterno: A_Paterno_Madre,
                    apellido_materno: A_Materno_Madre,
                    sexo: "Mujer"})

    MERGE (madre) -[:Padre]->(fallecido) RETURN madre', '', {fallecido:fallecido,
                            Nombre_Madre:Nombre_Madre,
                            A_Paterno_Madre:A_Paterno_Madre,
                            A_Materno_Madre:A_Materno_Madre}) yield value
WITH Estado_Civil, Categoria, Pareja_Nombre, Pareja_A_Paterno, Pareja_A_Materno, Pareja_Sexo, fallecido, padre, value.madre as madre
    CALL apoc.do.when(Categoria <> "Natural" AND padre IS NOT null AND madre IS NOT null,
    'MERGE (madre)-[:Casado]->(:Matrimonio:Evento)<-[:Casado]-(padre)', '', {madre:madre, padre:padre}) YIELD value
WITH Estado_Civil, Pareja_Nombre, Pareja_A_Paterno, Pareja_A_Materno, Pareja_Sexo, fallecido
    CALL apoc.do.when(Estado_Civil = 'Casado' and Pareja_Nombre + Pareja_A_Paterno + Pareja_A_Materno <> "",
    'CREATE
        (pareja:Registro:Persona:Defuncion {
            nombre: Pareja_Nombre,
            apellido_paterno: Pareja_A_Paterno,
            apellido_materno: Pareja_A_Materno,
            sexo: Pareja_Sexo
        })
    MERGE (pareja) -[:Casado]->(matrimonio:Evento:Matrimonio)<-[:Casado]-(fallecido)', '', {fallecido:fallecido,
                                Pareja_Nombre:Pareja_Nombre,
                                Pareja_A_Paterno:Pareja_A_Paterno,
                                Pareja_A_Materno:Pareja_A_Materno,
                                Pareja_Sexo:Pareja_Sexo}) YIELD value
RETURN 0
