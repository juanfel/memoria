match (p:Persona)-[r:Similitud]-(dup:Persona)
where exists(r.similitud_total)
WITH p, dup, r
MATCH (p)-[:Padre]->(h1:Persona)
MATCH (dup)-[:Padre]->(h2:Persona)
WITH p, h1, dup, h2, r
WHERE h1 <> h2
OPTIONAL MATCH path_sim=ShortestPath((h1)-[:Similitud*..6]-(h2))
OPTIONAL MATCH path_par=ShortestPath((h1)-[:Similitud_Apellidos*..6]-(h2))
//Escojo un camino de parentezco solo si se sabe que ambos hermanos no son la misma persona
WITH p, dup, h1, h2, r, 
	CASE WHEN path_sim is null and path_par is not null THEN path_par 
        WHEN path_sim is not null and path_par is null THEN path_sim 
        WHEN path_sim is not null and path_par is not null and all(x in relationships(path_sim) where exists(x.parentezco)) then path_sim 
        ELSE path_par end as path,
        //Quiero poder escoger si uso similitud_total o parentezco independiente del camino
        //porque puede ser que lo mas exacto es usar el camino de similitud pero tratandolo como de hermandad
        CASE WHEN path_sim is null and path_par is not null THEN true 
            WHEN path_sim is not null and path_par is null and all(x in relationships(path_sim) where exists(x.parentezco) )THEN true 
            WHEN path_sim is not null and path_par is not null and all(x in relationships(path_sim) where exists(x.parentezco))
                                                                and not all(x in relationships(path_sim) where exists(x.similitud_total))then true 
            ELSE false end as es_pariente
//Calculo los puntajes
WITH p, dup, path, h1, h2, r, es_pariente, CASE path WHEN null then 0 ELSE length(path) end as s,
	CASE WHEN path is null then [{similitud_total:0, parentezco:0}] ELSE relationships(path) end as rels
with p, dup, path, h1,h2, s, CASE WHEN es_pariente = false THEN 
                                        REDUCE(i = 0, x in rels | x.similitud_total + i)/size(rels)  ELSE 
                                        REDUCE(i = 0, x in rels | x.parentezco + i)/size(rels) end as s_avg, r, rels, es_pariente
SET r.similitud_hijos = coalesce(s_avg, coalesce(1/s, 0))
