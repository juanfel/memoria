MATCH (c:ComienzoReglas)-->(s:Regla)  
WHERE c.label = {NombreRegla}
MATCH r=(s)-[:Operacion*0..]->(q)-[rt:Operacion]->(t:ResultadoRegla)
WITH nodes(r) as node_list, relationships(r) as rel_list
unwind(node_list) as nds
unwind(rel_list) as rls
WITH DISTINCT nds, rls
WITH collect(DISTINCT nds) as nodes, collect(DISTINCT rls) as rels
RETURN nodes, rels
