MATCH (c:ComienzoReglas)-->(s:Regla)  
WHERE c.label = {NombreRegla}
MATCH r=(s)-[:Operacion*0..]->(q)-[rt:Operacion]->(t:ResultadoRegla)
return nodes(r) as nodos, relationships(r) as relaciones

