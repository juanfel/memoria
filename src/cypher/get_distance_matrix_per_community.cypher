MATCH (a:Persona), (b:Persona)
WHERE a.partition = b.partition
OPTIONAL MATCH (a)-[r]-(b)
WITH a, b,
    CASE
    WHEN exists(r.similitud_total) THEN r.similitud_total
    ELSE 0
    END as similitud
ORDER by id(b)
WITH a, b, collect(similitud) as sim_row, collect(id(b)) as row_ids
ORDER BY a.partition, a, b
UNWIND sim_row as row
WITH DISTINCT a.partition as part, collect(row) as matrix, size(collect(DISTINCT row_ids)) as node_count, collect(DISTINCT b) as nodes
return part, matrix, node_count, nodes
ORDER BY part
