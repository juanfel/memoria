CALL algo.unionFind('Persona', 'Similitud', {weightProperty:'similitud_total',threshold:$threshold, defaultValue:0.0, write:true, partitionProperty:"partition", concurrency:1})
YIELD nodes, setCount, loadMillis, computeMillis, writeMillis;
