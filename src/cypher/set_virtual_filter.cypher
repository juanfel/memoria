//Busca los subgrafos de una persona
MATCH (p:Persona_Arbol)
where toLower(p.nombre) =~ $nombre_re and toLower(p.apellido_paterno) =~ $apellido_paterno_re and
        toLower(p.apellido_materno) =~ $apellido_materno_re
CALL apoc.path.subgraphAll(p, {relationshipFilter:"Padre|Casado", labelFilter: "+Persona|Matrimonio"
                                ,maxLevel:$generaciones_mat}) yield nodes as mNodes, relationships as mRels
CALL apoc.path.subgraphAll(p, {relationshipFilter:"Padre", maxLevel:$generaciones}) yield nodes, relationships
//Clona los nodos involucrados
unwind mNodes + nodes as nds
WITH collect(DISTINCT nds) as node_list
CALL apoc.refactor.cloneNodes(node_list, True) yield input, output as n
call apoc.do.case(['Persona_Arbol' in labels(n) , "set n:Persona_Virtual\n remove n:Persona_Arbol", 'Evento_Matrimonio_Arbol' in labels(n),
                            "set n:Evento_Matrimonio_Virtual\n remove n:Evento_Matrimonio_Arbol"],"",{n:n}) yield value
return value
