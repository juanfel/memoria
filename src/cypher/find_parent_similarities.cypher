match (p:Persona)-[r:Similitud]-(dup:Persona)
where exists(r.similitud_total)
WITH p, dup, r
MATCH (p)<-[:Padre]-(h1)
MATCH (dup)<-[:Padre]-(h2)
WITH p, h1, dup, h2, r
WHERE h1.sexo = h2.sexo AND h1 <> h2
OPTIONAL MATCH path=ShortestPath((h1)-[:Similitud*..6]-(h2))
WITH p, dup, path, h1, h2, r, CASE path WHEN null then 0 ELSE length(path) end as s,
        CASE path WHEN null then [{similitud_total:0}] ELSE relationships(path) end as rels
with p, dup, path, h1, h2, s, case WHEN s > 0 THEN REDUCE(i = 0, x in rels | x.similitud_total + i)/size(rels)  
                                                ELSE 0 END as s_avg, r, rels
CALL apoc.do.when(h1.sexo = "Hombre" OR h2.sexo = "Hombre", "SET r.similitud_padre = s", "SET r.similitud_madre = s", {r:r, s:coalesce(s_avg, coalesce(1/s, 0))}) YIELD value
return p, dup, path, h1, h2, s, s_avg, r, rels
