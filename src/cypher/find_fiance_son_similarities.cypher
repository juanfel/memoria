MATCH (conyuge:Persona)-[:Casado]->(:Matrimonio)
        <-[:Casado]-(p:Persona)-[r:Similitud]-(dup:Persona)-[:Padre]->(:Persona)
        <-[:Padre]-(padre:Persona)
WHERE exists(r.similitud_total) and conyuge.sexo = padre.sexo
OPTIONAL MATCH path = ShortestPath((conyuge)-[:Similitud*..6]-(padre))
WITH conyuge, p, r, dup, padre, CASE path WHEN null then 0 ELSE length(path) end as s,
    CASE path WHEN null then [{similitud_total:0}] ELSE relationships(path) end as rels
WITH conyuge, p, r, dup, padre, case WHEN s > 0 THEN REDUCE(i = 0, x in rels | x.similitud_total + i)/size(rels)  
                                                ELSE 0 END as s_avg, rels, s
SET r.similitud_matrimonio = s_avg
return conyuge, p, r, dup, padre, s_avg, s
