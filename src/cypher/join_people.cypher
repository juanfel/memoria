// Filtra partiendo por las personas fallecidas pertenecientes a un clan
// De ahí busca candidatos entre los casados y bautizados
MATCH (persona:Fallecido)
WHERE toLower(persona.nombre) =~ ".*u(b|v)al.*"
WITH persona

OPTIONAL MATCH (alias_casado)-[r:Casado]-()
WHERE apoc.text.distance(alias_casado.nombre,persona.nombre) <= 3 //Nombres tienen que ser similares
AND r.fecha IS NOT NULL
AND apoc.date.parse(r.fecha,'s', 'dd-MM-yyyy') < apoc.date.parse(persona.fecha_deceso, 's', 'dd-MM-yyyy') //No pueden haber paradojas de tiempo
WITH alias_casado, persona

OPTIONAL MATCH (alias_bautizo:Bautizado)
WHERE apoc.text.distance(alias_bautizo.nombre, persona.nombre) <= 3
AND apoc.date.parse(alias_bautizo.fecha_bautizo, 's', 'dd-MM-yyyy') < apoc.date.parse(persona.fecha_deceso, 's', 'dd-MM-yyyy')
WITH alias_bautizo, alias_casado, persona

OPTIONAL MATCH (alias_padre:Padre:Madre)--(hijo_bautizado:Bautizado)
WHERE apoc.text.distance(alias_padre.nombre, persona.nombre) <= 3
AND size(hijo_bautizado.fecha_bautizo) = size('dd-MM-yyyy')
AND apoc.date.parse(hijo_bautizado.fecha_bautizo, 's', 'dd-MM-yyyy') < apoc.date.parse(persona.fecha_deceso, 's', 'dd-MM-yyyy')
WITH alias_padre, alias_bautizo, alias_casado, persona

OPTIONAL MATCH (alias_padre_matrimonio:Padre:Madre)--(hijo_casado:Persona)-[matrimonio:Casado]-()
WHERE apoc.text.distance(alias_padre.nombre, persona.nombre) <= 3
AND size(matrimonio.fecha) = size('dd-MM-yyyy')
AND apoc.date.parse(matrimonio.fecha, 's', 'dd-MM-yyyy') < apoc.date.parse(persona.fecha_deceso, 's', 'dd-MM-yyyy')
RETURN alias_bautizo, alias_casado, alias_padre, alias_padre_matrimonio, persona
