
CREATE (begin_rule:ComienzoReglas $regla_tiempos_coherentes)-[:InicioReglas]->(avr:Regla $var_AVR)
CREATE (avr)-[:Operacion {operacion:"&", funcion_membresia:"Muy_Viejo"}]
                    ->(:Regla:ResultadoRegla $res_coherencia_baja)
CREATE (avr)-[:Operacion {operacion:"&", funcion_membresia:"Paradoja"}]
                    ->(:Regla:ResultadoRegla $res_coherencia_baja)
CREATE (avr)-[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]
                ->(:Regla $var_FNI) -[:Operacion {operacion:"& ~", funcion_membresia:"Lejos"}]->
                (:Regla $var_MDE) -[:Operacion {operacion:"&", funcion_membresia:"p_n"}]->
                (:Regla $var_EAN) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAM) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAB) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_DEV) -[:Operacion {operacion:"&", funcion_membresia:"Lejos"}]->
                        (:Regla:ResultadoRegla $res_coherencia_alta)

CREATE (begin_rule)-[:InicioReglas]->(fni:Regla $var_FNI)
CREATE (fni)-[:Operacion {operacion:"&", funcion_membresia:"Lejos"}]
                    ->(:Regla:ResultadoRegla $res_coherencia_baja)
CREATE (fni)-[:Operacion {operacion:"&", funcion_membresia:"Cerca"}]
                ->(:Regla $var_AVR) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_MDE) -[:Operacion {operacion:"&", funcion_membresia:"p_n"}]->
                (:Regla $var_EAN) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAM) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAB) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_DEV) -[:Operacion {operacion:"&", funcion_membresia:"Lejos"}]->
                        (:Regla:ResultadoRegla $res_coherencia_alta)

CREATE (begin_rule)-[:InicioReglas]->(mde:Regla $var_MDE)
CREATE (mde)-[:Operacion {operacion:"&", funcion_membresia:"p_n"}]
                    ->(:Regla:ResultadoRegla $res_coherencia_baja)
// Tal vez no se considera dentro de las reglas para que no influya.
CREATE (mde)-[:Operacion {operacion:"&", funcion_membresia:"p_s"}]
                ->(:Regla $var_AVR) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_FNI) -[:Operacion {operacion:"&", funcion_membresia:"Lejos"}]->
                (:Regla $var_EAN) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAM) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAB) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_DEV) -[:Operacion {operacion:"&", funcion_membresia:"Lejos"}]->
                        (:Regla:ResultadoRegla $res_coherencia_alta)
CREATE (mde)-[:Operacion {operacion:"&", funcion_membresia:"n_s"}]
                ->(:Regla $var_AVR) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_FNI) -[:Operacion {operacion:"&", funcion_membresia:"Lejos"}]->
                (:Regla $var_EAN) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAM) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAB) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_DEV) -[:Operacion {operacion:"&", funcion_membresia:"Lejos"}]->
                        (:Regla:ResultadoRegla $res_coherencia_alta)
CREATE (begin_rule)-[:InicioReglas]->(ean:Regla $var_EAN)
CREATE (ean)-[:Operacion {operacion:"&", funcion_membresia:"Muy_Viejo"}]
                    ->(:Regla:ResultadoRegla $res_coherencia_baja)
CREATE (ean)-[:Operacion {operacion:"&", funcion_membresia:"Muy_Joven"}]
                    ->(:Regla:ResultadoRegla $res_coherencia_baja)
// Tal vez no se considera dentro de las reglas para que no influya.
CREATE (ean)-[:Operacion {operacion:"&", funcion_membresia:"Normal"}]
                ->(:Regla $var_AVR) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_FNI) -[:Operacion {operacion:"& ~", funcion_membresia:"Lejos"}]->
                (:Regla $var_MDE) -[:Operacion {operacion:"& ", funcion_membresia:"p_n"}]->
                (:Regla $var_EAM) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAB) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_DEV) -[:Operacion {operacion:"&", funcion_membresia:"Lejos"}]->
                        (:Regla:ResultadoRegla $res_coherencia_alta)
CREATE (begin_rule)-[:InicioReglas]->(eam:Regla $var_EAM)
CREATE (eam)-[:Operacion {operacion:"&", funcion_membresia:"Muy_Viejo"}]
                    ->(:Regla:ResultadoRegla $res_coherencia_baja)
CREATE (eam)-[:Operacion {operacion:"&", funcion_membresia:"Muy_Joven"}]
                    ->(:Regla:ResultadoRegla $res_coherencia_baja)
// Tal vez no se considera dentro de las reglas para que no influya.
CREATE (eam)-[:Operacion {operacion:"&", funcion_membresia:"Normal"}]
                ->(:Regla $var_AVR) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_FNI) -[:Operacion {operacion:"& ~", funcion_membresia:"Lejos"}]->
                (:Regla $var_MDE) -[:Operacion {operacion:"&", funcion_membresia:"p_n"}]->
                (:Regla $var_EAN) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAB) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_DEV) -[:Operacion {operacion:"&", funcion_membresia:"Lejos"}]->
                        (:Regla:ResultadoRegla $res_coherencia_alta)

CREATE (begin_rule)-[:InicioReglas]->(eab:Regla $var_EAB)
CREATE (eab)-[:Operacion {operacion:"&", funcion_membresia:"Muy_Viejo"}]
                    ->(:Regla:ResultadoRegla $res_coherencia_baja)
CREATE (eab)-[:Operacion {operacion:"&", funcion_membresia:"Muy_Joven"}]
                    ->(:Regla:ResultadoRegla $res_coherencia_baja)
// Tal vez no se considera dentro de las reglas para que no influya.
CREATE (eab)-[:Operacion {operacion:"&", funcion_membresia:"Normal"}]
                ->(:Regla $var_AVR) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_FNI) -[:Operacion {operacion:"& ~", funcion_membresia:"Lejos"}]->
                (:Regla $var_MDE) -[:Operacion {operacion:"&", funcion_membresia:"p_n"}]->
                (:Regla $var_EAN) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAM) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_DEV) -[:Operacion {operacion:"&", funcion_membresia:"Lejos"}]->
                        (:Regla:ResultadoRegla $res_coherencia_alta)

CREATE (begin_rule)-[:InicioReglas]->(dev:Regla $var_DEV)
CREATE (dev)-[:Operacion {operacion:"&", funcion_membresia:"Lejos"}]
                    ->(:Regla:ResultadoRegla $res_coherencia_baja)
CREATE (dev)-[:Operacion {operacion:"&", funcion_membresia:"Normal"}]
                ->(:Regla $var_AVR) -[:Operacion {operacion:"& ~", funcion_membresia:"Normal"}]->
                (:Regla $var_FNI) -[:Operacion {operacion:"& ~", funcion_membresia:"Lejos"}]->
                (:Regla $var_MDE) -[:Operacion {operacion:"&", funcion_membresia:"p_n"}]->
                (:Regla $var_EAN) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAM) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                (:Regla $var_EAB) -[:Operacion {operacion:"&", funcion_membresia:"Normal"}]->
                        (:Regla:ResultadoRegla $res_coherencia_alta)
