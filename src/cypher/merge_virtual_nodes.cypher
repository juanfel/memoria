MATCH (n:Persona_Arbol)
set n.nombre_list = n.nombre
set n.apellido_paterno_list = n.apellido_paterno
set n.apellido_materno_list = n.apellido_materno
WITH n.partition as part, collect(DISTINCT n) as node_list
call apoc.refactor.mergeNodes(node_list, {properties:{nombre_list:"combine", apellido_paterno_list:"combine",
                                                apellido_materno_list:"combine", `.`:"overwrite"},mergeRels:True}) yield node
return node
