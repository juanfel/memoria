match (p)-[r:Similitud]-(q)
where exists(r.similitud_padre) or exists(r.similitud_madre) or exists(r.similitud_hijos) or exists(r.similitud_matrimonio)
return p, q ,r
