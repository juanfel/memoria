MATCH path=(:Persona_Virtual)-[:Padre|Casado]-()
unwind nodes(path) as n
unwind relationships(path) as r
return collect(DISTINCT n) as nodes, collect(DISTINCT r) as rels
