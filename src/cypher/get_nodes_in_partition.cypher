MATCH path=(a:Persona {partition:$partition})-[:Similitud*0..1]-(b:Persona)
WHERE all(x in nodes(path) WHERE x.partition = $partition)
with nodes(path) as nodes, relationships(path) as rels
unwind nodes as node
unwind CASE when rels = [] THEN [null] ELSE rels END as rel
return collect(DISTINCT node) as nodes, collect(DISTINCT rel) as rels
