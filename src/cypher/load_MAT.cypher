LOAD CSV WITH HEADERS FROM "file:///all_MAT.csv" AS line
WITH line,
     coalesce(line.Marido_Nombre, "") as Nombre_Marido,
     coalesce(line.Marido_Apellido_Paterno, "") as Marido_Apellido_Paterno,
     coalesce(line.Marido_Apellido_Materno, "") as Marido_Apellido_Materno,
     coalesce(line.Marido_Padre_Nombre, "") as Marido_Nombre_Padre,
     coalesce(line.Marido_Padre_Apellido_Paterno, "") as Marido_Apellido_Paterno_Padre,
     coalesce(line.Marido_Padre_Apellido_Materno, "") as Marido_Apellido_Materno_Padre,
     coalesce(line.Marido_Madre_Nombre, "") as Marido_Nombre_Madre,
     coalesce(line.Marido_Madre_Apellido_Paterno, "") as Marido_Apellido_Paterno_Madre,
     coalesce(line.Marido_Madre_Apellido_Materno, "") as Marido_Apellido_Materno_Madre,
     coalesce(line.Mujer_Nombre, "") as Nombre_Mujer,
     coalesce(line.Mujer_Apellido_Paterno, "") as Mujer_Apellido_Paterno,
     coalesce(line.Mujer_Apellido_Materno, "") as Mujer_Apellido_Materno,
     coalesce(line.Mujer_Padre_Nombre, "") as Mujer_Nombre_Padre,
     coalesce(line.Mujer_Padre_Apellido_Paterno, "") as Mujer_Apellido_Paterno_Padre,
     coalesce(line.Mujer_Padre_Apellido_Materno, "") as Mujer_Apellido_Materno_Padre,
     coalesce(line.Mujer_Madre_Nombre, "") as Mujer_Nombre_Madre,
     coalesce(line.Mujer_Madre_Apellido_Paterno, "") as Mujer_Apellido_Paterno_Madre,
     coalesce(line.Mujer_Madre_Apellido_Materno, "") as Mujer_Apellido_Materno_Madre,

     coalesce(line.Padrino_1_Nombre, "") as Padrino_1_Nombre,
     coalesce(line.Padrino_1_Apellido_Paterno, "") as Padrino_1_Apellido_Paterno,
     coalesce(line.Padrino_1_Apellido_Materno, "") as Padrino_1_Apellido_Materno,
     coalesce(line.Padrino_1_Sexo, "") as Padrino_1_Sexo,
     coalesce(line.Padrino_2_Nombre, "") as Padrino_2_Nombre,
     coalesce(line.Padrino_2_Apellido_Paterno, "") as Padrino_2_Apellido_Paterno,
     coalesce(line.Padrino_2_Apellido_Materno, "") as Padrino_2_Apellido_Materno,
     coalesce(line.Padrino_2_Sexo, "") as Padrino_2_Sexo,
     coalesce(line.Testigo_1_Nombre, "") as Testigo_1_Nombre,
     coalesce(line.Testigo_1_Apellido_Paterno, "") as Testigo_1_Apellido_Paterno,
     coalesce(line.Testigo_1_Apellido_Materno, "") as Testigo_1_Apellido_Materno,
     coalesce(line.Testigo_1_Sexo, "") as Testigo_1_Sexo,
     coalesce(line.Testigo_2_Nombre, "") as Testigo_2_Nombre,
     coalesce(line.Testigo_2_Apellido_Paterno, "") as Testigo_2_Apellido_Paterno,
     coalesce(line.Testigo_2_Apellido_Materno, "") as Testigo_2_Apellido_Materno,
     coalesce(line.Testigo_2_Sexo, "") as Testigo_2_Sexo,
     coalesce(line.Fecha, "") as Fecha,
     coalesce(line.Impedimento_Consanguineidad, "") as Impedimento_Consanguineidad,
     coalesce(line.Lugar, "") as Lugar
CREATE 
//Crea los nodos de marido y mujer. Si en el registro alguno de ellos está vacío
//al menos podemos inferir que existe un marido o mujer
    (marido:Registro:Persona:Matrimonio:Casado { nombre: Nombre_Marido ,
                        apellido_paterno: Marido_Apellido_Paterno ,
                        apellido_materno: Marido_Apellido_Materno ,
                        estado_civil: "Casado",
                        sexo: "Hombre"
    }),
    (mujer:Registro:Persona:Matrimonio:Casado { nombre: Nombre_Mujer ,
                        apellido_paterno: Mujer_Apellido_Paterno ,
                        apellido_materno: Mujer_Apellido_Materno ,
                        estado_civil: "Casado",
                        sexo: "Mujer"
    }),
    (matrimonio:Evento:Matrimonio {lugar: Lugar,
                            fecha: Fecha,
                            impedimento_consanguineidad: Impedimento_Consanguineidad})

MERGE (marido) -[:Casado ]->(matrimonio)<-[:Casado]-(mujer)
//El resto los queremos crear solo cuando estos existan. Sino solo son ruido.
foreach( x IN CASE WHEN Marido_Nombre_Padre + Marido_Apellido_Paterno_Padre + Marido_Apellido_Materno_Padre <> ""
            THEN [1] ELSE [] END | 
            CREATE
                (padre_marido:Padre:Registro:Persona:Matrimonio {nombre: Marido_Nombre_Padre,
                                    apellido_paterno: Marido_Apellido_Paterno_Padre,
                                    apellido_materno: Marido_Apellido_Materno_Padre,
                                    sexo: "Hombre"
                })
            MERGE (padre_marido) -[:Padre]->(marido))

foreach( x IN CASE WHEN Marido_Nombre_Madre + Marido_Apellido_Paterno_Madre + Marido_Apellido_Materno_Madre <> ""
            THEN [1] ELSE [] END | 
            CREATE
            (madre_marido:Madre:Registro:Persona:Matrimonio {nombre: Marido_Nombre_Madre,
             apellido_paterno: Marido_Apellido_Paterno_Madre,
             apellido_materno: Marido_Apellido_Materno_Madre,
             sexo: "Mujer"
             })
             MERGE (madre_marido) -[:Padre]->(marido)
             )

foreach( x IN CASE WHEN Mujer_Nombre_Padre + Mujer_Apellido_Paterno_Padre + Mujer_Apellido_Materno_Padre <> ""
            THEN [1] ELSE [] END | 
            CREATE
            (padre_mujer:Padre:Registro:Persona:Matrimonio {nombre: Mujer_Nombre_Padre,
             apellido_paterno: Mujer_Apellido_Paterno_Padre,
             apellido_materno: Mujer_Apellido_Materno_Padre,
             sexo: "Hombre"
             })
             MERGE (padre_mujer) -[:Padre]->(mujer)
             )

foreach( x IN CASE WHEN Mujer_Nombre_Madre + Mujer_Apellido_Paterno_Madre + Mujer_Apellido_Materno_Madre <> ""
            THEN [1] ELSE [] END | 
            CREATE
            (madre_mujer:Madre:Registro:Persona:Matrimonio {nombre: Mujer_Nombre_Madre,
             apellido_paterno: Mujer_Apellido_Paterno_Madre,
             apellido_materno: Mujer_Apellido_Materno_Madre,
             sexo: "Mujer"
             })
            MERGE (madre_mujer) -[:Padre]->(mujer)
       )

foreach( x IN CASE WHEN Padrino_1_Nombre + Padrino_1_Apellido_Paterno + Padrino_1_Apellido_Materno <> ""
            THEN [1] ELSE [] END | 
            CREATE
    (padrino1:Padrino:Registro:Persona:Matrimonio {
                    nombre: Padrino_1_Nombre,
                    apellido_paterno: Padrino_1_Apellido_Paterno,
                    apellido_materno: Padrino_1_Apellido_Materno,
                    sexo: Padrino_1_Sexo
    })
    MERGE (padrino1)-[:Padrino]->(matrimonio)
    )
foreach( x IN CASE WHEN Padrino_2_Nombre + Padrino_2_Apellido_Paterno + Padrino_2_Apellido_Materno <> ""
            THEN [1] ELSE [] END | 
            CREATE
    (padrino2:Padrino:Registro:Persona:Matrimonio {
                    nombre: Padrino_2_Nombre,
                    apellido_paterno: Padrino_2_Apellido_Paterno,
                    apellido_materno: Padrino_2_Apellido_Materno,
                    sexo: Padrino_2_Sexo
    })
    MERGE (padrino2)-[:Padrino]-(matrimonio))
foreach( x IN CASE WHEN Testigo_1_Nombre + Testigo_1_Apellido_Paterno + Testigo_1_Apellido_Materno <> ""
            THEN [1] ELSE [] END | 
            CREATE
    (testigo1:Testigo:Registro:Persona:Matrimonio {
                    nombre: Testigo_1_Nombre,
                    apellido_paterno: Testigo_1_Apellido_Paterno,
                    apellido_materno: Testigo_1_Apellido_Materno,
                    sexo: Testigo_1_Sexo
    })
    MERGE (testigo1)-[:Testigo]->(matrimonio))
foreach( x IN CASE WHEN Testigo_2_Nombre + Testigo_2_Apellido_Paterno + Testigo_2_Apellido_Materno <> ""
            THEN [1] ELSE [] END | 
            CREATE
    (testigo2:Testigo:Registro:Persona:Matrimonio {
                    nombre: Testigo_2_Nombre,
                    apellido_paterno: Testigo_2_Apellido_Paterno,
                    apellido_materno: Testigo_2_Apellido_Materno,
                    sexo: Testigo_2_Sexo
    })
    MERGE (testigo2)-[:Testigo]->(matrimonio))
