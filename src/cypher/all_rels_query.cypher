MATCH r1=(p:Persona)-[rel]-(b)
WHERE (toFloat(rel.similitud_nombre) >= $threshold_nombre and toFloat(rel.coherencia_temporal) >= $threshold_tiempo) or (type(rel) <> "Similitud")
UNWIND nodes(r1) as n1
WITH n1, rel
ORDER BY n1.partition
RETURN collect(DISTINCT n1)  as nodes, collect(DISTINCT rel) as rels   
