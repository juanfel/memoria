match (:Persona)-[r:Similitud]-(:Persona)
where not exists(r.similitud_hijos) and not exists(r.similitud_madre) and
	not exists(r.similitud_padre) and not exists(r.similitud_matrimonio)
delete r
