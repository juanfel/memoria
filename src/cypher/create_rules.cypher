CREATE (nombres_similares:ComienzoReglas {label: "NombresSimilares"})
CREATE (mismo_apellido_paterno:Regla {variable:"ApellidoPaterno"})
CREATE (mismo_apellido_materno:Regla {variable:"ApellidoMaterno"})
CREATE (mismo_nombre:Regla {variable:"Nombres"})
CREATE (personas_similares:ResultadoRegla:Regla {consecuente:"Iguales[similar]"})
CREATE (personas_algo_similares:ResultadoRegla:Regla {consecuente:"Iguales[algo_similar]"})
CREATE (personas_no_similares:ResultadoRegla:Regla {consecuente:"Iguales[no_similar]"})
CREATE (probablemente_parientes:ResultadoRegla:Regla {consecuente:"Parientes[similar]"})
CREATE (algo_probablemente_parientes:ResultadoRegla:Regla {consecuente:"Parientes[algo_similar]"})
CREATE (probablemente_no_parientes:ResultadoRegla:Regla {consecuente:"Parientes[no_similar]"})

MERGE (nombres_similares)-[:InicioReglas]->(mismo_apellido_paterno)

//Rama de mismos apellidos paternos
MERGE (mismo_apellido_paterno)-[:Operacion {operacion:"&", funcion_membresia:"similar"}]->(mismo_apellido_materno)
MERGE (mismo_apellido_materno)-[:Operacion {operacion:"&", funcion_membresia:"similar"}]->(mismo_nombre)
MERGE (mismo_nombre)-[:Operacion {operacion:"&", funcion_membresia:"similar"}]->(personas_similares)
MERGE (mismo_nombre)-[:Operacion {operacion:"&", funcion_membresia:"similar"}]->(probablemente_parientes)
MERGE (mismo_nombre)-[:Operacion {operacion:"&", funcion_membresia:"algo_similar"}]->(personas_algo_similares)
MERGE (mismo_nombre)-[:Operacion {operacion:"&", funcion_membresia:"algo_similar"}]->(probablemente_parientes)
MERGE (mismo_nombre)-[:Operacion {operacion:"&", funcion_membresia:"no_similar"}]->(personas_no_similares)
MERGE (mismo_nombre)-[:Operacion {operacion:"&", funcion_membresia:"no_similar"}]->(probablente_parientes)
MERGE (mismo_apellido_materno)-[:Operacion {operacion:"&", funcion_membresia:"algo_similar"}]->(mismo_nombre)

CREATE (mismo_AP_maybe_AM_Nombres:Regla {variable:"Nombres"})
MERGE (mismo_AP_maybe_AM_Nombres)-[:Operacion {operacion:"&", funcion_membresia:"similar"}]->(personas_similares)
MERGE (mismo_AP_maybe_AM_Nombres)-[:Operacion {operacion:"&", funcion_membresia:"algo_similar"}]->(personas_algo_similares)
MERGE (mismo_AP_maybe_AM_Nombres)-[:Operacion {operacion:"&", funcion_membresia:"no_similar"}]->(personas_no_similares)
MERGE (mismo_apellido_materno)-[:Operacion {operacion:"&", funcion_membresia:"no_similar"}]->(mismo_AP_maybe_AM_Nombres)
//Lo siguiente lo hago porque si llega a apellido materno es porque son super similares
MERGE (mismo_apellido_materno)-[:Operacion {operacion:"&", funcion_membresia:"similar"}]->(probablemente_parientes)
MERGE (mismo_apellido_materno)-[:Operacion {operacion:"&", funcion_membresia:"algo_similar"}]->(probablemente_parientes)
MERGE (mismo_apellido_materno)-[:Operacion {operacion:"&", funcion_membresia:"no_similar"}]->(probablemente_parientes)

//Rama de apellidos paternos desconocido
CREATE (maybe_AP_mismo_AM:Regla {variable:"ApellidoMaterno"})
CREATE (maybe_AP_mismo_nombre:Regla {variable:"Nombres"})

MERGE (maybe_AP_mismo_AM)-[:Operacion {operacion:"&", funcion_membresia:"similar"}]->(maybe_AP_mismo_nombre)
MERGE (maybe_AP_mismo_nombre)-[:Operacion {operacion:"&", funcion_membresia:"similar"}]->(personas_algo_similares)
MERGE (maybe_AP_mismo_nombre)-[:Operacion {operacion:"&", funcion_membresia:"similar"}]->(probablemente_parientes)
MERGE (maybe_AP_mismo_nombre)-[:Operacion {operacion:"&", funcion_membresia:"algo_similar"}]->(personas_algo_similares)
MERGE (maybe_AP_mismo_nombre)-[:Operacion {operacion:"&", funcion_membresia:"algo_similar"}]->(algo_probablemente_parientes)
MERGE (maybe_AP_mismo_nombre)-[:Operacion {operacion:"&", funcion_membresia:"no_similar"}]->(personas_no_similares)
MERGE (maybe_AP_mismo_nombre)-[:Operacion {operacion:"&", funcion_membresia:"no_similar"}]->(algo_probablemente_parientes)
MERGE (maybe_AP_mismo_AM)-[:Operacion {operacion:"&", funcion_membresia:"algo_similar"}]->(probablemente_no_parientes)
MERGE (maybe_AP_mismo_AM)-[:Operacion {operacion:"&", funcion_membresia:"algo_similar"}]->(personas_no_similares)
MERGE (maybe_AP_mismo_AM)-[:Operacion {operacion:"&", funcion_membresia:"no_similar"}]->(probablemente_no_parientes)
MERGE (maybe_AP_mismo_AM)-[:Operacion {operacion:"&", funcion_membresia:"no_similar"}]->(personas_no_similares)

//Rama de apellidos paternos distintos
MERGE (mismo_apellido_paterno)-[:Operacion {operacion:"&", funcion_membresia:"no_similar"}]->(personas_no_similares)
MERGE (mismo_apellido_paterno)-[:Operacion {operacion:"&", funcion_membresia:"no_similar"}]->(probablemente_no_parientes)
