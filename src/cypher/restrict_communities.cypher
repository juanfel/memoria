MATCH (a:Persona)-[:Padre]-(b:Persona)
WHERE a.partition = b.partition
MATCH (a)-[ap:Similitud]-(p:Persona)-[pb:Similitud]-(b)
WHERE exists(ap.similitud_total) AND exists(pb.similitud_total)
WITH DISTINCT p as p_unique, a, b, ap, pb, 
    CASE
        WHEN ap.similitud_total > pb.similitud_total THEN ap
        WHEN pb.similitud_total > ap.similitud_total then pb
        WHEN rand() > 0.5 THEN ap
        ELSE pb
        END as menos_similar
DELETE menos_similar
