match p=(a:Persona)-[r:Similitud]-(a_dup:Persona)-[r_p:Similitud]-(a2:Persona)-[r1:Similitud*0..1]-(a3:Persona)-[:Padre]->(b:Persona)-[r2:Similitud*0..2]-(b2:Persona)-[:Padre]->(c:Persona)
WHERE c.partition = a.partition and a_dup.partition = a.partition and a2.partition = a.partition
and a3.partition = a.partition and b2.partition = b.partition
unwind r1 as r1s
unwind r2 as r2s
WITH  a,a_dup,r1s,r2s,r,r_p,b,c, CASE WHEN r.similitud_total < r_p.similitud_total THEN r
                              else r_p END as choice
WHERE exists(r1s.similitud_total) and exists(r2s.similitud_total)
AND exists(r.similitud_total) and exists(r_p.similitud_total)
WITH DISTINCT choice
SET choice.similitud_total = choice.similitud_total - 0.1
