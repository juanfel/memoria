LOAD CSV WITH HEADERS FROM "file:///all_BAUT.csv" AS line 
WITH line,
     coalesce(line.Nombre, "") as Nombre,
     coalesce(line.Apellido_Materno, "") as A_Materno,
     coalesce(line.Apellido_Paterno, "") as A_Paterno,
     coalesce(line.Sexo, "") as Sexo,
     coalesce(line.Padre_Nombre, "") as Nombre_Padre,
     coalesce(line.Padre_Apellido_Paterno, "") as A_Paterno_Padre,
     coalesce(line.Padre_Apellido_Materno, "") as A_Materno_Padre,
     coalesce(line.Madre_Nombre, "") as Nombre_Madre,
     coalesce(line.Madre_Apellido_Paterno, "") as A_Paterno_Madre,
     coalesce(line.Madre_Apellido_Materno, "") as A_Materno_Madre,
     coalesce(line.Categoría, "") as Categoria,
     coalesce(line.Edad, "") as Edad,
     coalesce(line.Lugar, "") as Lugar,
     coalesce(line.Fecha, "") as Fecha,
     coalesce(line.Padrino_1_Nombre, "") as Padrino_1_Nombre,
     coalesce(line.Padrino_1_Apellido_Paterno, "") as Padrino_1_Apellido_Paterno,
     coalesce(line.Padrino_1_Apellido_Materno, "") as Padrino_1_Apellido_Materno,
     coalesce(line.Padrino_1_Sexo, "") as Padrino_1_Sexo,
     coalesce(line.Padrino_2_Nombre, "") as Padrino_2_Nombre,
     coalesce(line.Padrino_2_Apellido_Paterno, "") as Padrino_2_Apellido_Paterno,
     coalesce(line.Padrino_2_Apellido_Materno, "") as Padrino_2_Apellido_Materno,
     coalesce(line.Padrino_2_Sexo, "") as Padrino_2_Sexo
CREATE 
//Crea los nodos
    (bebe:Bautizado:Registro:Persona:Bautizo {
                    nombre: line.Nombre,
                    apellido_paterno: A_Paterno,
                    apellido_materno: A_Materno,
                    lugar_bautizo: Lugar,
                    fecha_bautizo: Fecha,
                    sexo: Sexo,
                    edad_bautizo: Edad}),
    (padre:Padre:Registro:Persona:Bautizo {
                    nombre: Nombre_Padre,
                    apellido_paterno: A_Paterno_Padre,
                    apellido_materno: A_Materno_Padre,
                    sexo: "Hombre"}),
    (madre:Madre:Registro:Persona:Bautizo {
                    nombre: Nombre_Madre,
                    apellido_paterno: A_Paterno_Madre,
                    apellido_materno: A_Materno_Madre,
                    sexo: "Mujer"})
//Crea las relaciones
MERGE (padre) -[:Padre]->(bebe)
MERGE (madre) -[:Padre]->(bebe)

//Si el hijo es legitimo crea la relación de matrimonio
foreach(x in CASE WHEN Categoria <> "Natural" then [1] else [] END | 
    MERGE (padre)-[:Casado]->(:Matrimonio:Evento)<-[:Casado]-(madre))
//Los padrinos pueden o no estar
foreach( x IN CASE WHEN Padrino_1_Nombre + Padrino_1_Apellido_Paterno + Padrino_1_Apellido_Materno <> ""
            THEN [1] ELSE [] END | 
            CREATE
    (padrino1:Padrino:Registro:Persona:Matrimonio {
                    nombre: Padrino_1_Nombre,
                    apellido_paterno: Padrino_1_Apellido_Paterno,
                    apellido_materno: Padrino_1_Apellido_Materno,
                    sexo: Padrino_1_Sexo
    })
    MERGE (padrino1)-[:Padrino]->(bebe)
    )
foreach( x IN CASE WHEN Padrino_2_Nombre + Padrino_2_Apellido_Paterno + Padrino_2_Apellido_Materno <> ""
            THEN [1] ELSE [] END | 
            CREATE
    (padrino2:Padrino:Registro:Persona:Matrimonio {
                    nombre: Padrino_2_Nombre,
                    apellido_paterno: Padrino_2_Apellido_Paterno,
                    apellido_materno: Padrino_2_Apellido_Materno,
                    sexo: Padrino_2_Sexo
    })
    MERGE (padrino2)-[:Padrino]-(bebe))
