MATCH (n)-[r]-(p)
WITH n,r,p
WHERE ((n:Persona_Arbol or n:Evento_Matrimonio_Arbol ) and not (p:Persona_Arbol or p:Evento_Matrimonio_Arbol) and n <> p)
OR ((n:Persona_Arbol or n:Evento_Matrimonio_Arbol) and (p:Persona_Arbol or p:Evento_Matrimonio_Arbol) AND (type(r) = "Similitud" or type(r) = "Similitud_Apellidos"))
DELETE r
