MATCH (p0 :Persona_Virtual)
WHERE toLower(p0.nombre) =~ $nombre_re and toLower(p0.apellido_paterno) =~ $apellido_paterno_re and
    toLower(p0.apellido_materno) =~ $apellido_materno_re
MATCH (p1 :Persona_Virtual)
WHERE     toLower(p1.nombre) =~ $nombre_re1 and toLower(p1.apellido_paterno) =~ $apellido_paterno_re1 and
        toLower(p1.apellido_materno) =~ $apellido_materno_re1
        AND p0 <> p1
WITH p0, p1
WHERE p0 <> p1
MATCH path=ShortestPath((p0)-[:Padre*..]-(p1))
return nodes(path) as nodes, relationships(path) as rels
