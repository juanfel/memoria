CREATE (francisca:Persona:Registro:Casado:Padre { nombre: 'Francisca', 
                        apellido_paterno: "Díaz de León",
                        apellido_materno: "",
                        sexo: "Mujer"
}),

 (bernabe:Persona:Registro:Padre { nombre: 'Bernabé',
                        apellido_paterno: "Díaz de León",
                        sexo: "Hombre"}),
 (baltazara:Persona:Registro:Madre { nombre: "Baltazara",
                                apellido_paterno: "de Oses",
                                apellido_materno: "Guzmán",
                                sexo: "Mujer"
                                }),
                        
 (francisco:Persona:Registro:Casado:Madre { nombre: "Francisco",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Hombre"
                                }),
 (agustin:Persona:Registro { nombre: "Agustín",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Hombre",
                                vivo_en: "01/01/1757"}),
 (rosa:Persona:Registro { nombre: "Rosa",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Mujer",
                                vivo_en: "01/01/1757"}),
 (mateo:Persona:Registro { nombre: "Mateo",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Hombre",
                                vivo_en: "01/01/1757"}),
 (matias:Persona:Registro { nombre: "Matías",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Hombre", 
                                vivo_en: "01/01/1757"}),
 (josefa:Persona:Registro { nombre: "Josefa",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Mujer",
                                vivo_en: "01/01/1757"}),
 (pascual:Persona:Registro { nombre: "Pascual",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Hombre",
                                vivo_en: "01/01/1757"}),
 (maria:Persona:Registro { nombre: "María",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Mujer", 
                                vivo_en: "01/01/1757"})
WITH francisca, francisco, baltazara, bernabe, agustin, rosa, mateo, matias, josefa, pascual, maria
MERGE (francisca)-[:Casado]->(:Matrimonio)<-[:Casado]-(francisco)
MERGE (baltazara)-[:Padre]->(francisca)<-[:Padre]-(bernabe)
MERGE (francisca)-[:Padre]->(agustin)<-[:Padre]-(francisco)
MERGE (francisca)-[:Padre]->(rosa)<-[:Padre]-(francisco)
MERGE (francisca)-[:Padre]->(mateo)<-[:Padre]-(francisco)
MERGE (francisca)-[:Padre]->(matias)<-[:Padre]-(francisco)
MERGE (francisca)-[:Padre]->(josefa)<-[:Padre]-(francisco)
MERGE (francisca)-[:Padre]->(pascual)<-[:Padre]-(francisco)
MERGE (francisca)-[:Padre]->(maria)<-[:Padre]-(francisco)
