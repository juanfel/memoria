CREATE (pascual:Registro:Padre:Persona:Casado { nombre: "Pascual",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Hombre"
                                }),
(francisco:Padre:Registro:Persona:Casado { nombre: "Francisco",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Hombre"
                                }),
(francisca:Madre:Registro:Persona:Casado { nombre: "Francisca",
                                apellido_paterno: "Díaz de León",
                                apellido_materno: "",
                                sexo: "Mujer"
                                }),
(maria_alcaino:Madre:Registro:Persona:Casado { nombre: "María",
                                apellido_paterno: "Alcaíno",
                                apellido_materno: "",
                                sexo: "Mujer"
                                }),
(francisco_alcaino:Padre:Registro:Persona:Casado { nombre: "Francisco",
                                apellido_paterno: "Alcaíno",
                                apellido_materno: "",
                                sexo: "Hombre"
                                }),
(maria_gonzales:Madre:Registro:Persona:Casado { nombre: "María",
                                apellido_paterno: "Gonzáles",
                                apellido_materno: "",
                                sexo: "Mujer"
                                }),
(fernando:Persona:Registro:Casado { nombre: "Fernando",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Hombre",
                                vivo_en: "01/01/1774"
                                }),
(rosa_alegria:Persona:Registro:Casado { nombre: "Rosa",
                                apellido_paterno: "Alegría",
                                apellido_materno: "",
                                sexo: "Mujer",
                                vivo_en: "01/01/1774"
                                }),
(gerardo:Persona:Registro:Casado { nombre: "Gerardo",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Hombre",
                                vivo_en: "01/01/1774"}),
(josefa_aguilar:Persona:Registro:Casado { nombre: "Josefa",
                                apellido_paterno: "Aguilar",
                                apellido_materno: "",
                                sexo: "Mujer",
                                vivo_en: "01/01/1774"}),
(cayetano:Persona:Registro { nombre: "Cayetano",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Hombre",
                                vivo_en: "01/01/1774"}),
(teodoro:Persona:Registro { nombre: "Teodoro",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Hombre",
                                vivo_en: "01/01/1774"}),
(martin:Persona:Registro { nombre: "Martín",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Hombre",
                                vivo_en: "01/01/1774"}),
(cecilia:Persona:Registro:Casado { nombre: "Cecilia",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Mujer",
                                vivo_en: "01/01/1774"}),
(gerardo_andrada:Persona:Registro:Casado { nombre: "Gerardo",
                                apellido_paterno: "Andrada",
                                apellido_materno: "",
                                sexo: "Hombre",
                                vivo_en: "01/01/1774"}),
(maria_josefa:Persona:Registro:Casado { nombre: "María Josefa",
                                apellido_paterno: "Basoalto",
                                apellido_materno: "",
                                sexo: "Mujer",
                                vivo_en: "01/01/1774"}),
(vicente_avila:Persona:Registro:Casado { nombre: "Vicente",
                                apellido_paterno: "Ávila",
                                apellido_materno: "",
                                sexo: "Hombre",
                                vivo_en: "01/01/1774"})
WITH pascual, francisco, francisca, maria_alcaino, francisco_alcaino, maria_gonzales, fernando, rosa_alegria,
    gerardo, josefa_aguilar, cayetano, teodoro, martin, cecilia, gerardo_andrada, maria_josefa, vicente_avila
    
MERGE (pascual)-[:Casado]->(:Matrimonio)<-[:Casado]-(maria_alcaino)
MERGE (francisco)-[:Padre]->(pascual)<-[:Padre]-(francisca)
MERGE (francisco)-[:Casado]->(:Matrimonio)<-[:Casado]-(francisca)

MERGE (francisco_alcaino)-[:Padre]->(maria_alcaino)<-[:Padre]-(maria_gonzales)
MERGE (francisco_alcaino)-[:Casado]->(:Matrimonio)<-[:Casado]-(maria_gonzales)

MERGE (pascual)-[:Padre]->(fernando)-[:Padre]-(maria_alcaino)
MERGE (fernando)-[:Casado]->(:Matrimonio)-[:Casado]-(rosa_alegria)


MERGE (pascual)-[:Padre]->(gerardo)<-[:Padre]-(maria_alcaino)
MERGE (gerardo)-[:Casado]->(:Matrimonio)<-[:Casado]-(josefa_aguilar)

MERGE (pascual)-[:Padre]->(cayetano)<-[:Padre]-(maria_alcaino)
MERGE (pascual)-[:Padre]->(teodoro)<-[:Padre]-(maria_alcaino)
MERGE (pascual)-[:Padre]->(martin)<-[:Padre]-(maria_alcaino)

MERGE (pascual)-[:Padre]->(cecilia)<-[:Padre]-(maria_alcaino)
MERGE (cecilia)-[:Casado]->(:Matrimonio)<-[:Casado]-(gerardo_andrada)

MERGE (pascual)-[:Padre]->(maria_josefa)<-[:Padre]-(maria_alcaino)
MERGE (maria_josefa)-[:Casado]->(:Matrimonio)<-[:Casado]-(vicente_avila)
