MATCH (a:Persona)-[:Casado]->(e:Evento:Matrimonio)<-[:Casado]-(b:Persona)
WITH e, max(a.partition, b.partition) as partition
SET e.partition = partition
SET e:Evento_Matrimonio
