"""
En éste modulo se encuentra todo aquello que tenga que ver con el análisis
de personas dentro de la BDG.
"""
from datetime import datetime
import attr as attr
from graph_connector import GraphConnector
from fuzzy import FuzzyRules, FuzzyFactory
import numpy as np
import py_stringmatching as sm
from dateutil.relativedelta import relativedelta
from persona import Persona, Matrimonio
import math
import io

@attr.s
class Analizer(object):
    """Clase encargada de analizar los datos del grafo para
    encontrar resultados
    """
    default_fuzzy_rules = FuzzyFactory.create_test_fuzzyrules()
    fuzzy_names = FuzzyFactory.create_name_fuzzyrules()
    graph_connector = attr.ib()

    @graph_connector.default
    def graph_connector_default(self):
        return GraphConnector("localhost:7474", "neo4j", "Neo4j")

    fuzzy_rules = attr.ib(default=default_fuzzy_rules)

    def analizar(self, nombre=".*"):
        """TODO: Por ahora hace el query de prueba y lo devuelve
        TODO: Hacer que devuelva personas completas

        :nombre: TODO
        :returns: TODO

        """
        self.graph_connector.eliminar_similitudes()
        self.analizar_nombres(nombre, threshold_nombre=0.7)
        self.analizar_tiempos(nombre, threshold_nombre=0.7, threshold_tiempo=0.5)

    def analizar_grafo(self,
                       decision_default="Conservar",
                       eliminar_sin_score=True,
                       fuzzy_defaults=None):
        """
        Analiza los nodos en busca de similitudes en la estructura
        del grafo.
        """
        if decision_default != "Rechazar":
            decision_default = "Conservar" ## Si acepto o rechazo por default

        if fuzzy_defaults is None:
            fuzzy_defaults = FuzzyFactory.default_graph_vars

        self.graph_connector.do_graph_sim_queries()
        resultados = self.graph_connector.get_graph_sim_scores()
        fuzzy_rules = FuzzyFactory.create_graph_fuzzyrules()
        fuzzy_rules.procesar_string_reglas(reglas=FuzzyFactory.graph_rule_strings)

        for res in resultados:
            p = Persona.wrap(res['p'])
            q = Persona.wrap(res['q'])
            r = res['r']

            inputs = {}
            inputs = {'Padre': r['similitud_padre'],
                      'Madre': r['similitud_madre'],
                      'Conyuge': r['similitud_matrimonio'],
                      'Hijo': r['similitud_hijos']
                      }
            inputs = {k:(v if v is not None else 0.5) for k, v in inputs.items() }
            try:
                decision = fuzzy_rules.ejecutar_reglas(inputs, "Decision")
                decision_string = fuzzy_rules.encontrar_membresia_output(decision, "Decision")
                print(decision_string)
                if decision_string == "Default":
                    decision_string = decision_default
            except Exception as e:
                print(e)
                decision_string = decision_default

            if decision_string == "Rechazar":
                p.similares.remove(q)
                self.graph_connector.push_to_graph(p)
                print("Eliminando relación {0} con {1} \n\t {2}".format(p.nombre_completo, q.nombre_completo, inputs))
            else:
                print("Aceptando {0} con {1} \n\t inputs: {2}".format(p.nombre_completo, q.nombre_completo, inputs))

        if eliminar_sin_score:
            self.graph_connector.delete_rels_with_no_graph_sims()

    def analizar_nombres(self,
                         nombre=".*",
                         threshold_nombre=0.65,
                         opciones_nombre={}):
        """
        Realiza el analisis de nombres para todos los miembros de un
        grafo.
        :param nombre: Nombre o Apellido por el cual filtrar.
        """
        log_output = io.StringIO()
        self.graph_connector.eliminar_similitudes()
        self.fuzzy_names.procesar_arbol_decision(self.graph_connector, "NombresSimilares")
        resultados = self.graph_connector.query_de_prueba(nombre)
        tx = self.graph_connector.graph
        for r in resultados:
            p = Persona.wrap(r['p'])
            p.estimar_apellido_materno()
            p_dup = Persona.wrap(r['p_duplicate'])
            p_dup.estimar_apellido_materno()
            resultados_nombre, similitud, parentezco = self.similitud_por_nombres(p, p_dup, **opciones_nombre)
            if similitud >= threshold_nombre or math.isclose(similitud, threshold_nombre):
                print(p_dup.nombre_completo + "," + p.nombre_completo + ", " +
                       ' ' + str(similitud) + "\n", file=log_output)
                print("\t {0} {1}\n".format(
                    self.fuzzy_names.encontrar_membresia_output(similitud, "Iguales"),
                    self.fuzzy_names.encontrar_membresia_output(parentezco, "Parientes")), file=log_output)
                print("\t resultados por separado: {0}\n".format(resultados_nombre), file=log_output)
                p.similares.update(p_dup, similitud_nombre=similitud, parentezco=parentezco
                                )
                tx.push(p)
            elif parentezco and parentezco >= 0.767:
                p.parientes.update(p_dup, parentezco=parentezco)
                tx.push(p)
        log = log_output.getvalue()
        log_output.close()
        return log

    def analizar_tiempos(self, nombre=".*", threshold_nombre=0.65, threshold_tiempo=0.5, defaults=None):
        """
        Se encarga de analizar las concordancias de tiempo entre dos personas
        relacionadas por nombre.
        """
        log_output = io.StringIO()
        self.fuzzy_times = FuzzyFactory.create_time_fuzzyrules(defaults=defaults)
        self.fuzzy_times.procesar_arbol_decision(self.graph_connector, "TiemposCoherentes")
        resultados = self.graph_connector.get_similars(nombre)
        for r in resultados:
            p = Persona.wrap(r['persona'])
            for similar in p.similares:
                if not p.similares.get(similar,"coherencia_temporal") or p.similares.get(similar,"coherencia_temporal") >= 0.5:
                    assert p.eventos_padrino_bautizo.related_class is not Matrimonio
                    assert similar.eventos_padrino_bautizo.related_class is not Matrimonio
                    resultados_tiempo = self.similitud_por_tiempos(p, similar)
                    try:
                        coherencia = self.fuzzy_times.ejecutar_reglas(resultados_tiempo, "CoherenciaTemporal")
                        similitud_nombre = p.similares.get(similar, "similitud_nombre")
                        if (coherencia < threshold_tiempo and not math.isclose(coherencia, threshold_tiempo)) or \
                                similitud_nombre < threshold_nombre and not math.isclose(similitud_nombre, threshold_nombre):
                            print("A Eliminar: {0} {1} por coherencia: {2}\n".format(p.nombre_completo, similar.nombre_completo, coherencia), file=log_output)
                            print("{0}\n".format(resultados_tiempo), file=log_output)
                            # p.similares.update(similar, coherencia_temporal=coherencia)
                        else:
                            similitud_total = (0.5*similitud_nombre + 0.5*coherencia)
                            p.similares.update(similar, coherencia_temporal=coherencia, similitud_total=similitud_total)
                        self.graph_connector.push_to_graph(p)
                    except:
                        print(resultados_tiempo)
        print('done')
        log = log_output.getvalue()
        log_output.close()
        return log

    def similitud_por_tiempos(self, persona1: Persona, persona2: Persona):
        """
        Compara dos personas y obtiene las distancias entre los posibles tiempos
        """
        persona1.estimar_nacimiento()
        persona2.estimar_nacimiento()
 
        resultados = {"MDE": 3,
                      "EAM": 25,
                      "EAB": 25,
                      "EAN": 25}
        resultados['AVR'] = self.comparar_edades_relativas(persona1, persona2)
        resultados['FNI'] = self.comparar_fechas_nacimiento(persona1, persona2)
        resultados['MDE'] = self.comparar_murio_despues_evento(persona1, persona2)
        resultados['EAM'] = self.comparar_nacio_antes_evento(persona1, persona2)
        resultados['EAN'] = self.comparar_nacio_antes_hijos(persona1, persona2)
        resultados['DEV'] = self.comparar_entre_eventos(persona1, persona2)
        return resultados

    def comparar_fechas_nacimiento(self, persona1: Persona, persona2: Persona):
        """
        Verifica que al menos las fechas de nacimiento sean
        cercanas
        """
        try:
            delta = relativedelta(persona1.fecha_nacimiento, persona2.fecha_nacimiento)
            return min(abs(delta.years), 200)
        except:
            return 0

    def comparar_edades_relativas(self, persona1: Persona, persona2: Persona):
        """
        Compara la fecha de muerte de uno y la fecha de nacimiento de
        otro.
        """
        try:
            if persona1.fecha_nacimiento and persona2.fecha_entierro:
                edad = persona1.estimar_edad_a_la_fecha(persona2.fecha_entierro)
            elif persona2.fecha_nacimiento and persona1.fecha_entierro:
                edad = persona2.estimar_edad_a_la_fecha(persona1.fecha_entierro)
            else:
                edad = 25
            if abs(edad.years) < 200:
                return edad.years
            elif edad.years < 0:
                return -200
            else:
                return 200
        except:
            edad = 25
            return edad

    def comparar_murio_despues_evento(self, persona1: Persona, persona2: Persona):
        """
        Compara si una persona murio despues de un evento al
        cual asistió otra persona
        """
        try:
            eventos_p1 = persona1.fechas_eventos_asistidos()
            eventos_p2 = persona2.fechas_eventos_asistidos()
            if eventos_p1 and persona2.fecha_entierro:
                delta = relativedelta(persona2.fecha_entierro, eventos_p1[0]).years
            elif eventos_p2 and persona1.fecha_entierro:
                delta = relativedelta(persona1.fecha_entierro, eventos_p2[0]).years
            else:
                delta = 3
            if abs(delta) < 200:
                return delta
            else:
                return 200
        except:
            return 3

    def comparar_nacio_antes_evento(self, persona1: Persona, persona2: Persona):
        """
        Compara si una persona murio despues de un evento al cual asistió otra persona
        """
        try:
            eventos_p1 = persona1.fechas_matrimonios_asistidos() + persona1.fechas_bautizos_asistidos()
            eventos_p2 = persona2.fechas_matrimonios_asistidos() + persona2.fechas_bautizos_asistidos()
            if eventos_p1 and persona2.fecha_nacimiento:
                return relativedelta(eventos_p1[0], persona2.fecha_nacimiento).years
            elif eventos_p2 and persona1.fecha_nacimiento:
                return relativedelta(eventos_p2[0], persona1.fecha_nacimiento).years
            else:
                return 25
        except:
            return 25

    def comparar_nacio_antes_hijos(self, persona1: Persona, persona2: Persona):
        """
        Compara si una persona nacio antes o despues de algún hijo de la otra
        persona
        """
        try:
            eventos_p1 = persona1.fechas_nacimiento_hijos()
            eventos_p2 = persona2.fechas_nacimiento_hijos()
            if eventos_p1 and persona2.fecha_nacimiento:
                return relativedelta(eventos_p1[0], persona2.fecha_nacimiento).years
            elif eventos_p2 and persona1.fecha_nacimiento:
                return relativedelta(eventos_p2[0], persona1.fecha_nacimiento).years
            else:
                return 25
        except:
            return 25

    def comparar_entre_eventos(self, persona1: Persona, persona2: Persona):
        """
        Compara las fechas a las cuales asistieron dos personas
        para ver que tan posible es de que haya sucedido así.
        """
        eventos_p1 = persona1.fechas_eventos_asistidos()
        eventos_p2 = persona2.fechas_eventos_asistidos()
        try:

            def f_normalizacion(x):
                return min(abs(x), 200)
            delta = relativedelta(eventos_p1[0], eventos_p2[0])
            return f_normalizacion(delta.years)
        except Exception as e:
            return 0

    def similitud_por_nombres(self,
                              persona1: Persona,
                              persona2: Persona,
                              comparer=sm.OverlapCoefficient().get_sim_score,
                              tokenizer=sm.QgramTokenizer):
        """
        Compara mediante reglas difusas a dos personas
        y devuelve su grado de similitud
        """
        campos_nombres = {"Nombres": "nombre",
                          "ApellidoPaterno": "apellido_paterno",
                          "ApellidoMaterno": "apellido_materno"
                          }
        resultados = dict()

        for campo in campos_nombres:
            resultados[campo] = self.comparar_nombres(getattr(persona1, campos_nombres[campo]),
                                                     getattr(persona2, campos_nombres[campo]),
                                                      comparer=comparer,
                                                      tokenizer=tokenizer
                                                      )
        try:
            similitud = self.fuzzy_names.ejecutar_reglas(resultados, "Iguales")
            parentezco = self.fuzzy_names.ejecutar_reglas(resultados, "Parientes")
        except Exception as e:
            print(str(resultados) + " " +
                            str(persona1.nombre_completo) + " " +
                            str(persona2.nombre_completo))
            raise
        return resultados, similitud, parentezco

    @classmethod
    def comparar_nombres(cls, nombre1, nombre2, comparer=sm.Cosine(), tokenizer=sm.QgramTokenizer):
        """Compara los dos nombres usando alguna metrica.
        Devuelve su valor.

        :nombre1: Nombre
        :nombre2: Nombre
        :comparer: Función comparadora
        :returns: Valor de semejanza entre los dos nombres

        """
        if nombre1 == "" or nombre2 == "":
            return 0.5
        tokenizer = tokenizer()
        if Analizer.es_apodo(nombre1, nombre2):
            return 1

        tokens1 = tokenizer.tokenize(nombre1)
        tokens2 = tokenizer.tokenize(nombre2)

        return comparer(tokens1, tokens2)

    @classmethod
    def es_apodo(cls, nombre1: str, nombre2: str):
        """
        Devuelve verdadero si encuentra que nombre1 y nombre2
        son apodos. Hace la comparación en minuscula para que
        no sea case sensitive.
        """
        apodos = [
            {"peña y lillo", "peñailillo", "peñalillo"},
            {"ubal", "duval"},
            {"león", "díaz de león"}
        ]

        for apodo in apodos:
            if nombre1.lower() in apodo and nombre2.lower() in apodo:
                return True
        return False


class identity_tokenizer(object):
    """
    Sirve para "tokenizar" en casos donde no haya necesidad de hacerlo.
    """
    def tokenize(self, token):
        return token
