"""
Modulo para calcular clusters dentro de las comunidades de
un gráfo.
"""

from graph_connector import GraphConnector
from scipy import array
from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
from scipy.spatial.distance import squareform
from matplotlib import pyplot as plt
from persona import Persona

class Clustering(object):
    """
    Tiene los métodos para clusterizar comunidades.
    """

    def __init__(self):
        self.connector = GraphConnector("localhost:7474", "neo4j", "Neo4j")

    def get_vectors(self):
        cursor = self.connector.get_distance_vector()
        vectors = []
        for part in cursor:
            vectors.append(dict(part))
        return vectors

    def get_linkage(self):
        """
        Calcula el linkage clustering de cada partición
        """
        vectors = self.get_vectors()
        parts = []
        linkages = []
        node_counts = []
        nodes = []
        for part in vectors:
            matrix = array(part['matrix']).reshape(part['node_count'], -1)
            node_counts.append(part['node_count'])
            nodes.append(part['nodes'])
            parts.append(matrix)
            try:
                linkages.append(linkage(squareform(matrix), method='weighted'))
            except ValueError as e:
                print(e)
                linkages.append(0)
        return parts, linkages, node_counts, nodes

    def get_flat_cluster(self, clusters, cluster_threshold=0.4, function='inconsistent'):
        """
        Obtiene una lista plana de clusters, los cuales son los que van a ir a
        la base de datos
        """
        flattened = fcluster(clusters, cluster_threshold, function, depth=3)
        return flattened

    def update_clusters_bd(self, cluster_threshold=0.4, function='inconsistent'):
        """
        Envía los datos de los clusters nuevos a la bdg
        """
        parts, clusters, node_counts, nodes = self.get_linkage()
        for i, c in enumerate(clusters):
            if(len(nodes[i]) > 1):
                flat_c = self.get_flat_cluster(c, cluster_threshold, function)
                for j, cluster_num in enumerate(flat_c):
                    # Cada todas las listas anteriores estan ordenadas por:
                    # particion y por nodo
                    persona = Persona.wrap(nodes[i][j])
                    new_part = "{0}-{1}".format(persona.partition, cluster_num)
                    persona.partition = new_part
                    self.connector.push_to_graph(persona)


    def show_dendrogram(self):
        p, l, c, ids = self.get_linkage()
        fig = plt.figure(figsize=(25, 10))
        dn = dendrogram(l[10])
        plt.show()
        return dn
