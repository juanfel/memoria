"""
Implementa todo lo que tiene que ver con las reglas fuzzy
"""
import skfuzzy as fuzz
import skfuzzy.control as ctrl
import numpy as np
import re
import parser
from graph_connector import GraphConnector


class FuzzyRules:
    """
    Clase base para controlar a todo el conjunto de reglas.
    Asume que las variables están en una escala de 0 a 1 y además sus funciones
    de membresía son triangulares equilateras.
    :param antecedents: Lista de antecedentes a usar, contiene un nombre y
    opcionalmente un rango y una lista con las funciones de membresía.
    Ej: {'nombre':'distancia_geografica','rango':np.linspace(0,10),
    'membresia': [("label",[0,0,0.5]), ...]
    :type antecedents: dict
    :param consequents: Similar a antecedents
    :type antecedents: dict
    :param rules: Lista de reglas de tipo if-then. Cada regla tiene
    que ser una tupla que contenga el antecedente y el consecuente.
    Ej:[("antecedente[miembro] | antecedente[miembro]", "consecuente[miembro]"]
    """

    def __init__(self, antecedents, consequents, rules=[], **kwargs):
        self.antecedents = dict()
        self.consequents = dict()
        self.default_range = np.linspace(0, 1)

        if 'default_mf' in kwargs:
            default_mf = kwargs['default_mf']
            self.default_member_functions = default_mf
        else:
            self.default_member_functions = [('no_similar', [0, 0, 0.5]),
                                             ('algo_similar', [0, 0.5, 1]),
                                             ('similar', [0.5, 1, 1])]
        self.default_member_shape = fuzz.trimf
        for ant in antecedents:
            if 'rango' in ant:
                universe = np.linspace(*ant['rango'])
            else:
                universe = self.default_range

            antecedent = ctrl.Antecedent(universe, ant['nombre'])

            members = self.default_member_functions
            if 'membresia' in ant:
                members = ant['membresia']
            for member in members:
                member_shape = self.default_member_shape
                if 'shape' in ant and member[0] in ant['shape']:
                    member_shape = ant['shape'][member[0]]
                if isinstance(member[1], dict):
                    antecedent[member[0]] = member_shape(antecedent.universe,
                                                         **member[1])
                else:
                    antecedent[member[0]] = member_shape(antecedent.universe,
                                                         member[1])
            self.antecedents[ant['nombre']] = antecedent

        for con in consequents:
            if 'rango' in con:
                universe = con['rango']
            else:
                universe = self.default_range

            members = self.default_member_functions

            if 'defuzzify_method' in con:
                defuzzify_method = con['defuzzify_method']
            else:
                defuzzify_method = 'centroid'
            consequent = ctrl.Consequent(universe, con['nombre'], defuzzify_method=defuzzify_method)

            if 'membresia' in con:
                members = con['membresia']
            for member in members:
                member_shape = self.default_member_shape
                if 'shape' in con and member[0] in con['shape']:
                    member_shape = con['shape'][member[0]]
                consequent[member[0]] = member_shape(consequent.universe,
                                                    member[1])
            self.consequents[con['nombre']] = consequent

    def make_rules(self, rule, name_of_type):
        """
        Crea una regla a partir de un string en la forma
        "variable[membresia] operador variable[membresia]"
        """
        operators = ["&", "|", "(", ")"]
        var_and_member_function_re = re.compile(r'(.*)\[(.*)\]')
        rule_list = rule.split()
        rule_functions = list()
        negate = False
        for var in rule_list:
            if var in operators:
                rule_functions.append(var)
            elif var == "~":
                negate = True
                continue
            else:
                var_separated = var_and_member_function_re.match(var)
                type_dict = name_of_type
                var_label = var_separated.group(1)
                var_member_name = var_separated.group(2)
                new_var = "self.{0}['{1}']['{2}']".format(
                    type_dict, var_label, var_member_name)
                if negate is True:
                    new_var = "~" + new_var
                    print(new_var)
                rule_functions.append(new_var)
                negate = False
        return str.join(" ", rule_functions)

    def procesar_string_reglas(self, reglas: list):
        """
        Procesa una lista de de tuplas (antecedente, consecuente) con strings con las reglas en el mismo formato
        en el cual se transforman los árboles de decisión y los prepara
        para ser ejecutados.
        """
        self.rules = []
        for ant, con in reglas:
            ant_transformed = self.make_rules(ant, "antecedents")
            con_transformed = self.make_rules(con, "consequents")

            ant_parsed = parser.compilest(parser.expr(ant_transformed))
            con_parsed = parser.compilest(parser.expr(con_transformed))

            try:
                true_rule = ctrl.Rule(antecedent=eval(ant_parsed),
                                      consequent=eval(con_parsed))
            except Exception:
                print(ant_transformed)
                print(con_transformed)
                raise
            self.rules.append(true_rule)

        self.similarity_ctrl = ctrl.ControlSystem(self.rules)
        self.similarity = ctrl.ControlSystemSimulation(self.similarity_ctrl)

    def procesar_arbol_decision(self, connector: GraphConnector, label: str):
        """
        Obtiene los nodos y relacioes del arbol de decisiones y luego
        lo transforma en las reglas a usar.
        """
        rules = connector.get_rules(label)
        self.rules = list()
        for rule in rules:
            antecedents = list()
            nodos = rule['nodos']
            rels = rule['relaciones']

            for n, r in zip(nodos[:-1], rels):
                if nodos.index(n) <= len(nodos) - 3:
                    antecedents.append("{0}[{1}] {2} ".format(
                        n["variable"], r["funcion_membresia"], r["operacion"]))
                else:
                    antecedents.append("{0}[{1}]".format(
                        n["variable"], r["funcion_membresia"]))

            consecuent = self.make_rules(nodos[-1]["consecuente"],
                                         "consequents")
            label = nodos[-1]["label"]

            antecedents = self.make_rules("".join(antecedents), "antecedents")
            parsed_rule = (parser.compilest(parser.expr(antecedents)),
                           parser.compilest(parser.expr(consecuent)))

            # Se que eval es maligno pero veré como lo hago
            # La idea es que las reglas vienen exactamente como las procesa
            # para crear la regla.
            try:
                true_rule = ctrl.Rule(
                    antecedent=eval(parsed_rule[0]),
                    consequent=eval(parsed_rule[1]))
            except Exception:
                print(consecuent)
                print(antecedents)
                raise
            self.rules.append(true_rule)

        self.similarity_ctrl = ctrl.ControlSystem(self.rules)
        self.similarity = ctrl.ControlSystemSimulation(self.similarity_ctrl)

    def ejecutar_reglas(self, inputs, nombre_output):
        """
        Hace correr las reglas con los valores de input ingresados.

        :param inputs: Son todos los inputs relevantes para el cálculo. Van
        en un dict con el nombre como llave acompañado de su valor. Ej: ["var":value]
        :type inputs: dict
        """

        for inp in inputs:
            self.similarity.input[inp] = inputs.get(inp)

        self.similarity.compute()

        return self.similarity.output[nombre_output]

    def encontrar_membresia_output(self, output, nombre_output):
        """
        Encuentra a que valor fuzzy corresponde un output.

        :output: Valor "crisp" de los resultados de las reglas
        :nombre_output: Label del resultado que se quiere
        :returns: Nombre del set al que pertenece

        """
        output_a_buscar = self.consequents[nombre_output]
        resultado_dict = dict()
        for out in output_a_buscar.terms:
            resultado_dict[out] = fuzz.interp_membership(
                output_a_buscar.universe, output_a_buscar[out].mf, output)
        return max(resultado_dict, key=resultado_dict.get)


class FuzzyFactory(object):
    """
    Su único objetivo es instanciar distintos tipos de
    controladores fuzzy ya creados.
    """

    @classmethod
    def create_test_fuzzyrules(cls):
        """
        Crea el controlador fuzzy que he estado usando de prueba.
        """
        ruleset = FuzzyRules(
            antecedents=[{
                'nombre': 'Caca',
                'membresia': [("Grande", [0, 0, 1, 1])],
                'shape': {
                    "Grande": fuzz.trapmf
                }
            }, {
                'nombre': "Poto",
                'membresia': [("Gordo", [0, 0, 1, 1])],
                'shape': {
                    "Gordo": fuzz.trapmf,
                }
            }, {
                'nombre':
                "Pichi",
                'membresia': [("Asquerosa", [0, 0, 0.5, 0.5]),
                              ("Con_Sangre", [0.4, 0.5, 1, 1])],
                'shape': {
                    "Asquerosa": fuzz.trapmf,
                    "Con_Sangre": fuzz.trapmf
                }
            }],
            consequents=[{
                'nombre': 'En_la',
                'membresia': [("caca", [0, 0.5, 1]), ("pasta", [0.5, 1, 1])]
            }],
            rules=[])

        return ruleset

    @classmethod
    def create_name_fuzzyrules(cls):
        """
        Crea el controlador lógico para manejar la similitud
        por nombres. Decide en base al nombre y apellido por separado.
        """
        ruleset = FuzzyRules(
            antecedents=[{
                'nombre': 'Nombres'
            }, {
                'nombre': 'ApellidoPaterno'
            }, {
                'nombre': 'ApellidoMaterno'
            }],
            consequents=[{
                'nombre': 'Iguales'
            }, {
                'nombre': 'Hermanos'
            }, {
                'nombre': 'Parientes'
            }],
            default_mf=[("mp_ns", [0, 0, 0.3]),
                        ("p_ns", [0, 0.3, 0.5]),
                        ("ns", [0.4, 0.6, 0.7]),
                        ("p_s", [0.5, 0.8, 1]),
                        ("mp_s", [0.8, 1, 1])])
        return ruleset

    ## Los defaults para las variables difusas del tiempo
    default_time_vars = {
        "AVR": {
            "default": 25,
            "rango": [-200, 200],
            "membresia": {"Muy_Viejo": [80, 120, 200, 200],
                          "Normal": [-1, 0, 80, 120],
                          "Paradoja": [-200, -200, -1, 0]}
        },
        "FNI": {
            'default': 0,
            'rango': [0, 200],
            "membresia": {"Lejos": [0, 10, 200, 200],
                          "Cerca": [0, 0, 10]
                          }
        },
        "MDE": {
            'default': 3,
            'rango': [-200, 200],
            "membresia": {"p_s": [0, 3, 200, 200],
                          "n_s": [-3, 0, 3],
                          "p_n": [-200, -200, -3, 0]}
        },
        "EAM": {
            'default': 25,
            'rango': [-200, 200],
            'membresia': {"Muy_Viejo": [50, 60, 200, 200],
                          "Normal": [20, 25, 50, 60],
                          "Muy_Joven": [-200, -200, 0, 25]
                          }
        },
        "EAB": {
            'default': 25,
            'rango': [-200, 200],
            'membresia': {"Muy_Viejo": [50, 60, 200, 200],
                          "Normal": [18, 18, 50, 60],
                          "Muy_Joven": [-200, -200, 0, 25]
                          }
        },
        "EAN": {
            'default': 25,
            'rango': [-200, 200],
            'membresia': {"Muy_Viejo": [50, 60, 200, 200],
                          "Normal": [16, 20, 50, 60],
                          "Muy_Joven": [-200, -200, 0, 18]
                          }
        },
        "DEV": {
            "default": 0,
            "rango": [0, 200],
            "membresia": {"Normal": {"a": 25, "b": 50},
                          "Lejos": {"a": 25, "b": 50}}
        },
        "CoherenciaTemporal": {
            "default": 0.5,
            "rango": [0, 1],
            "membresia": {"Baja": [0, 0, 1],
                         "Alta": [0, 1, 1]}
        }

    }

    @classmethod
    def create_time_fuzzyrules(cls, defaults=None):
        """
        Crea el controlador lógico para manejar la coherencia
        temporal de distintos eventos.
        """
        if defaults is None:
            defaults = cls.default_time_vars

        ruleset = FuzzyRules(
            antecedents=[
                {
                    'nombre': 'AVR',
                    'rango': defaults['AVR']['rango'],
                    'membresia': [("Muy_Viejo", defaults['AVR']['membresia']['Muy_Viejo']),
                                  ("Normal", defaults['AVR']['membresia']['Normal']),
                                  ("Paradoja", defaults['AVR']['membresia']['Paradoja'])],
                    'shape': {'Muy_Viejo': fuzz.trapmf,
                              'Normal': fuzz.trapmf,
                              'Paradoja': fuzz.trapmf}

                },
                {
                    'nombre': 'FNI',
                    'rango': defaults['FNI']['rango'],
                    'membresia': [("Lejos", defaults['FNI']['membresia']['Lejos']),
                                  ("Cerca", defaults['FNI']['membresia']['Cerca'])],
                    'shape': {"Lejos": fuzz.trapmf,
                              "Cerca": fuzz.trimf}
                },
                {
                    'nombre': 'MDE',
                    'rango': defaults['MDE']['rango'],
                    'membresia': [("p_s", defaults['MDE']['membresia']['p_s']),
                                  ("n_s", defaults['MDE']['membresia']['n_s']),
                                  ("p_n", defaults['MDE']['membresia']['p_n'])],
                    'shape': {"p_s": fuzz.trapmf,
                              "p_n": fuzz.trapmf}
                },
                {
                    'nombre': 'EAM',
                    'rango': defaults['EAM']['rango'],
                    'membresia': [("Muy_Viejo", defaults['EAM']['membresia']['Muy_Viejo']),
                                  ("Normal", defaults['EAM']['membresia']['Normal']),
                                  ("Muy_Joven", defaults['EAM']['membresia']['Muy_Joven'])],
                    'shape': {"Muy_Viejo": fuzz.trapmf,
                              "Normal": fuzz.trapmf,
                              "Muy_Joven": fuzz.trapmf
                              }
                },
                {
                    'nombre': 'EAB',
                    'rango': defaults['EAB']['rango'],
                    'membresia': [("Muy_Viejo", defaults['EAB']['membresia']['Muy_Viejo']),
                                  ("Normal", defaults['EAB']['membresia']["Normal"]),
                                  ("Muy_Joven", defaults['EAB']['membresia']["Muy_Joven"])],
                    'shape': {"Muy_Viejo": fuzz.trapmf,
                              "Normal": fuzz.trapmf,
                              "Muy_Joven": fuzz.trapmf
                              }
                },
                {
                    'nombre': 'EAN',
                    'rango': defaults['EAN']['rango'],
                    'membresia': [("Muy_Viejo", defaults['EAN']['membresia']["Muy_Viejo"]),
                                  ("Normal", defaults['EAN']['membresia']["Normal"]),
                                  ("Muy_Joven", defaults['EAN']['membresia']["Muy_Joven"])],
                    'shape': {"Muy_Viejo": fuzz.trapmf,
                              "Normal": fuzz.trapmf,
                              "Muy_Joven": fuzz.trapmf
                              }
                },
                {
                    'nombre': 'DEV',
                    'rango': defaults['DEV']['rango'],
                    'membresia': [("Normal", defaults['DEV']['membresia']["Normal"]),
                                  ("Lejos", defaults['DEV']['membresia']["Lejos"])],
                    'shape':{"Normal": fuzz.zmf,
                             "Lejos": fuzz.smf}
                }
            ],
            consequents=[
                {
                    'nombre': 'CoherenciaTemporal',
                    'membresia': [("Baja", defaults['CoherenciaTemporal']['membresia']["Baja"]),
                                  ("Alta", defaults['CoherenciaTemporal']['membresia']["Alta"])],
                    'defuzzify_method': 'som'
                }
            ]
        )
        return ruleset

    ## Defaults para variables difusas de grafo
    default_graph_vars = {
        "Padre": {
            "default": 0.5,
            "rango": [0, 1],
            "membresia": {
                "Cerca": [0, 1, 1],
                "Lejos": [0, 0, 1]

            }

        },
        "Madre": {
            "default": 0.5,
            "rango": [0, 1],
            "membresia": {
                "Cerca": [0, 1, 1],
                "Lejos": [0, 0, 1]

            }

        },
        "Conyuge": {
            "default": 0.5,
            "rango": [0, 1],
            "membresia": {
                "Cerca": [0, 1, 1],
                "Lejos": [0, 0, 1]

            }

        },
        "Hijo": {
            "default": 0.5,
            "rango": [0, 1],
            "membresia": {
                "Cerca": [0, 1, 1],
                "Lejos": [0, 0, 1]

            }

        },
        "Decision": {
            "default": 0.5,
            "rango": [0, 1],
            "membresia": {
                "Conservar": [0, 1, 1],
                "Default": [0, 0.5, 1],
                "Rechazar": [0, 0, 1]
            }

        },
    }
    @classmethod
    def create_graph_fuzzyrules(cls, defaults=None):
        """
        Crea las reglas difusas para comparar dos personas
        en base a las relaciones que tiene con padres, hermanos, conyuges
        etc.
        """
        if defaults is None:
            defaults = cls.default_graph_vars
        ruleset = FuzzyRules(
            antecedents=[
                {
                    'nombre': 'Padre',
                    'rango': defaults['Padre']['rango'],
                    'membresia': [("Cerca", defaults['Padre']['membresia']['Cerca']),
                                  ("Lejos", defaults['Padre']['membresia']['Lejos'])],
                    'shape': {'Cerca': fuzz.trimf,
                              'Lejos': fuzz.trimf}
                },
                {
                    'nombre': 'Madre',
                    'rango': defaults['Madre']['rango'],
                    'membresia': [("Cerca", defaults['Madre']['membresia']['Cerca']),
                                  ("Lejos", defaults['Madre']['membresia']['Lejos'])],
                    'shape': {'Cerca': fuzz.trimf,
                              'Lejos': fuzz.trimf}
                },
                {
                    'nombre': 'Conyuge',
                    'rango': defaults['Conyuge']['rango'],
                    'membresia': [("Cerca", defaults['Conyuge']['membresia']['Cerca']),
                                  ("Lejos", defaults['Conyuge']['membresia']['Lejos'])],
                    'shape': {'Cerca': fuzz.trimf,
                              'Lejos': fuzz.trimf}
                },
                {
                    'nombre': 'Hijo',
                    'rango': defaults['Hijo']['rango'],
                    'membresia': [("Cerca", defaults['Hijo']['membresia']['Cerca']),
                                  ("Lejos", defaults['Hijo']['membresia']['Lejos'])],
                    'shape': {'Cerca': fuzz.trimf,
                              'Lejos': fuzz.trimf}
                }],
        consequents=[
            {
                'nombre': 'Decision',
                'rango': defaults['Decision']['rango'],
                'membresia': [("Conservar", defaults['Decision']['membresia']['Conservar']),
                              ("Default", defaults['Decision']['membresia']['Default']),
                              ("Rechazar", defaults['Decision']['membresia']['Rechazar'])],
                'shape': {'Conservar': fuzz.trimf,
                          'Default': fuzz.trimf,
                          'Rechazar': fuzz.trimf},
                'defuzzify_method': 'som'
            },
        ])
        return ruleset

    graph_rule_strings = [
        ("Padre[Cerca] & Madre[Cerca]", "Decision[Conservar]"),
        ("Padre[Lejos] | Madre[Lejos]", "Decision[Rechazar]"),
        ("Hijo[Cerca] & Conyuge[Cerca]", "Decision[Conservar]"),
        ("Hijo[Lejos] & Padre[Cerca] & Madre[Cerca]", "Decision[Conservar]"),
        ("( Padre[Cerca] & Madre[Lejos] ) | ( Madre[Cerca] & Padre[Lejos] )", "Decision[Default]"),
        ("Hijo[Lejos] & Conyuge[Lejos] & ( ~ Padre[Cerca] & ~ Madre[Cerca] )", "Decision[Rechazar]"),
        ("( ~ Hijo[Cerca] | ~ Conyuge[Cerca] ) & ( Padre[Cerca] & Madre[Cerca] )", "Decision[Conservar]"),
        ("( Hijo[Lejos] & Conyuge[Cerca] ) | ( Hijo[Cerca] & Conyuge[Lejos] )", "Decision[Default]"),
        ("Conyuge[Cerca]", "Decision[Conservar]"),
        ("Hijo[Cerca]", "Decision[Conservar]"),
        ("Conyuge[Lejos]", "Decision[Rechazar]")
    ]
