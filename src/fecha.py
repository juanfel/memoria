from datetime import datetime
from py2neo.ogm import Property
from dateutil.relativedelta import relativedelta

class Fecha(Property):
    """
    Clase para representar fechas usando datetime obtenidas
    desde neo4j.
    """
    def __init__(self, key=None):
        super().__init__(key)

    def __get__(self, instance, owner):
        fecha = super().__get__(instance, owner)
        try:
            return datetime.strptime(fecha, "%d-%m-%Y")
        except:
            try:
                return datetime.strptime(fecha, "%d/%m/%Y")
            except:
                return fecha

    def __set__(self, instance, value):
        if type(value) is datetime and value is not None:
            super().__set__(instance, value.strftime("%d-%m-%Y"))
        else:
            super().__set__(instance, value)

class Edad(Property):
    """
    Permite a la edad calcular su propio relativedelta.
    """

    def __init__(self, key=None):
        super().__init__(key)
        self.customvalue = CustomString("")

    @classmethod
    def get_relativedelta(cls, value):
        try:
            valor, unidad = value.split()
            if unidad == "años" or unidad == "año":
                return relativedelta(years=int(valor))
            elif unidad == "meses":
                return relativedelta(months=int(valor))
            else:
                return None
        except:
            return None

    def __get__(self, obj, type):
        value = super().__get__(obj, type)
        if value:
            customvalue = CustomString(value, relativedelta=Edad.get_relativedelta(value))
        else:
            customvalue = None
        return customvalue

class CustomString(object):
    """
    Forma de representar un año de tal modo que se comporte como su string
    para la mayoría de los casos pero que permita obtener el delta
    """
    def __init__(self, valor, relativedelta=None):
        self.valor = valor
        self.relativedelta = relativedelta

    def __str__(self):
        return self.valor
    def __repr__(self):
        return "{0} {1}".format(self.valor, self.relativedelta)
    def __add__(self, other):
        return self.valor + other

