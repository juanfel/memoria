from abc import ABC, abstractmethod
from typing import List, TypeVar, Dict, Optional
from persona import Persona
from dateutil.relativedelta import relativedelta
from py2neo import Node
import datetime


class Importador(ABC):
    """
    Interfáz para cada uno de los importadores de personas.
    """

    @abstractmethod
    def transformar_a_persona(self, persona: Persona) -> Persona:
        """
        Se encarga de rellenar datos faltantes de persona en base
        a lo que se sabe de ella.
        Debe entregar una persona.
        """
        return persona

    @abstractmethod
    def crear_persona(self, registro: Node) -> Persona:
        """
        Crea una persona en base a un registro
        """
        persona = Persona.wrap(registro)
        return persona

    @classmethod
    def transformar_fecha(cls, fecha: str) -> datetime:
        """
        Transforma el string de una fecha "dd-mm_YYYY" a un datetime
        """

        return datetime.strptime(fecha, "%d-%m-%Y")


class ImportadorBautizos(Importador):
    """
    Importa los registros de bautizo y los transforma
    en personas
    """
    def transformar_a_persona(self, persona: Persona) -> Persona:
        """
        Se encarga de inferir cosas acerca de la persona obtenida
        del registro de Bautizo.
        """
        if persona.edad_bautizo is not None:
            persona.fecha_nacimiento = self.obtener_fecha_nacimiento(persona.fecha_bautizo,
                                                                     persona.edad_bautizo)
        return persona

    def obtener_fecha_nacimiento(self, fecha: datetime,
                                 edad_bautizo: Optional[int]) -> datetime:
        """
        Hace los calculos necesarios para estimar una fecha de nacimiento.
        """
        if edad_bautizo is not None:
            return relativedelta(fecha, years=-edad_bautizo)
        else:
            return relativedelta(fecha, years=-1)

    def crear_persona(self, registro: Dict[TypeVar('T')]) -> Persona:
        """
        Crea una persona a partir del registro. No infiere los otros
        datos. Asume que es un registro de bautizo.
        """
        persona = super.crear_persona(registro)
        return self.transformar_a_persona(persona)
