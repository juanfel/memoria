var data
function get_nodes(rule_json, isPartition){
    var nodes = new vis.DataSet(rule_json.nodes)
    var edges = new vis.DataSet(rule_json.edges)

    data = {
        nodes: nodes,
        edges: edges
    }
    draw(isPartition)
}
function draw(isPartition) {
    var container = document.getElementById('nodos');
    let hierarchical_opts = false;
    if (!isPartition) {
        hierarchical_opts = {
                sortMethod: "directed",
                levelSeparation: 500,
                nodeSpacing: 50,
                treeSpacing: 200
        }
    };
    var options = {
        layout: {
            improvedLayout: false,
            hierarchical: hierarchical_opts
        },
        physics: {
            enabled: false,
            stabilization: false
                },
        // configure: {
        //     filter: function (option, path) {
        //         if (path.indexOf('hierarchical') !== -1) {
        //             return true;
        //         }
        //         return false;
        //     },
        //     showButton:false
        // },
        edges: {
            arrows: "to"
        }
    };
    network = new vis.Network(container, data, options);

    var text_container = document.getElementById('info_rel')
    text_container.innerText = "Parametros"
    network.on("selectEdge", function(params){
        var text_container = document.getElementById('info_rel');
        var text = ""
        params.edges.forEach(function(element){
            var edge = data.edges.get(element);
            if(edge !== null && edge !== undefined){
                edge.label = edge.label_hidden;
                text += JSON.stringify(edge.properties);
                data.edges.update(edge);
            }
            else if(element.includes("clusterEdge") === true){
                //Hay que expandir el vertice del cluster para incluir todos sus labels
                var edgeIds = network.clustering.getBaseEdges(element);
                var labels = []
                edgeIds.forEach(function(id){
                    var edge = data.edges.get(id);
                    if(edge.label === null || edge.label === undefined){
                        labels.push(edge.label_hidden);
                    }
                })
                //Si no encuentro el edge que representa al cluster
                //Busca dentro de todos los vertices internos
                clusterEdge = network.clustering.clusteredEdges[element]
                if(clusterEdge !== null && clusterEdge !== undefined){
                    network.clustering.updateEdge(element, {label: labels.join()})
                }
                else {
                    edgeIds.forEach(function(id){
                        
                        clusterEdge = network.clustering.clusteredEdges[id];
                        if(clusterEdge !== null && clusterEdge !== undefined){
                            network.clustering.updateEdge(id, {label: labels.join()})
                        }
                    })
                }
            }
        })
        text_container.innerText = text
    });
    network.on("deselectEdge", function(params){
        params.previousSelection.edges.forEach(function(element){
            var edge = data.edges.get(element);
            if(edge !== null){
                edge.label = null;
                data.edges.update(edge);
            }
            else if(element.includes("clusterEdge") === true){
                clusterEdge = network.clustering.clusteredEdges[element];
                if(clusterEdge !== null && clusterEdge !== undefined){
                    network.clustering.updateEdge(element, {label: null})
                }
                else{
                    var edgeIds = network.clustering.getBaseEdges(element);
                    edgeIds.forEach(function(id){
                        network.clustering.updateEdge(id, {label: null})
                    })
                }
            }
        })
    });
    network.on("selectNode", function(params){
        var text_container = document.getElementById('info_nodo')
        var text = ""
        params.nodes.forEach(function(element){
            if(network.isCluster(element)){
                clusterNodes = network.getNodesInCluster(element);
                clusterNodes.forEach(function(element){
                    
                    var node = data.nodes.get(element)

                    if(node !== undefined && node !== null && node.properties !== undefined){
                    text += JSON.stringify(node.properties)
                    }
                })
            }
            else{
                var node = data.nodes.get(element)

                if(node !== undefined && node !== null && node.properties !== undefined){
                    text += JSON.stringify(node.properties)
                    if(!isPartition){
                        button = document.getElementById('partition_button').children[0];
                        button.href = Flask.url_for('print_nodes_in_partition', {'partition':node.properties.partition})
                    }
                }
            }
        })
        text_container.innerText = text
    });

    // createClustering(network, data)
}

function createClustering(network, data) {
    // Crea la visualización de clusters 
    var cluster = 0;
    var clusterOptions;
    data.nodes.forEach(function(nodo){
        if(cluster === 0){
            clusterOptions = {
                joinCondition:function(nodeOptions){
                    if(nodeOptions.properties !== undefined && nodeOptions.properties.partition !== undefined){
                        return nodeOptions.properties.partition === 0;
                    }
                    else{
                        return false;
                    }
                },
                processProperties: function (clusterOptions, childNodes, childEdges) {
                    node = data.nodes.get(childNodes[0]);
                    if(node.properties !== undefined){
                        clusterOptions.properties = node.properties
                    }
                    clusterOptions.mass = childNodes.length;
                    return clusterOptions
                }
            };
        }
        if( nodo.properties !== undefined &&
            nodo.properties.partition !== undefined && 
            nodo.properties.partition !== cluster
            ){
            // Al cambiar el numero de cluster significa que todos los que tienen
            // dicho numero ya pasaron, por lo tanto se crea la comunidad y se prepara
            // para crear el nuevo grupo
            network.cluster(clusterOptions);
            cluster = nodo.properties.partition;
            clusterOptions = {
                clusterEdgeProperties:{
                    arrows:'to'
                },
                joinCondition:function(nodeOptions){
                    if(nodeOptions.properties !== undefined && nodeOptions.properties.partition !== undefined){
                        return nodeOptions.properties.partition === cluster;
                    }
                    else{
                        return false;
                    }
                },
                processProperties: function (clusterOptions, childNodes, childEdges) {
                    node = childNodes[0];
                    if(node.properties !== undefined){
                        clusterOptions.properties = node.properties;
                        clusterOptions.label = node.label
                    }
                    clusterOptions.mass = childNodes.length;

                    childEdges.forEach(function(edge){
                        var og_edge = data.edges.get(edge.id)
                        og_edge.label = edge.label_hidden
                    })
                    return clusterOptions
                }
            };
        }
    })
}

