function draw(rule_json) {
    var container = document.getElementById('reglas');
    var data = rule_json;
    var options = {
                    layout: {
                        hierarchical: {
                            direction: "UD",
                            sortMethod: "directed"
                        }
                    }
                };
    network = new vis.Network(container, data, options);

    var text_container = document.getElementById('texto')
    text_container.innerText = "Parametros"
    network.on("selectEdge", function(params){
        var nodoId = params.edges[0];
        var nodo = data.edges.find(x => x.id === nodoId)
        var text_container = document.getElementById('texto')
        text_container.innerText = JSON.stringify(nodo.properties)
    });
}

