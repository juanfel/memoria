from datetime import datetime
from py2neo.ogm import Property, Related, RelatedTo, RelatedFrom, GraphObject, Label
from fecha import Fecha, Edad
from dateutil.relativedelta import relativedelta


class Persona(GraphObject):

    """Representa a una persona con todos sus
    atributos. Se instancia a partir de un registro de
    la base de datos de grafos. Calcula todo aquello
    que no esté explícito en el registro.

    #Fechas
    Las fechas se calculan a partir de los registros, los cuales siempre
    se asume que ocurrieron. El resto se calcula de tal forma de dar un valor
    razonable de acuerdo a la información disponible. Por ejemplo, si se sabe
    que una persona fue bautizada cuando tenía 3 meses se puede asumir que nació
    aproximadamente cerca de 3 meses antes.

    :fecha_nacimiento: Fecha donde se estima que nació una persona.
    :fecha_bautizo: Fecha donde se realizó el bautizo.
    :fecha_entierro: Fecha donde se realizó el entierro
    :fecha_muerte: Fecha donde se estima que murió una persona.
    """

    nombre = Property('nombre')
    apellido_paterno = Property('apellido_paterno')
    apellido_materno = Property('apellido_materno')
    partition = Property('partition')
    categoria = Property('categoría')
    sexo = Property('Sexo')
    matrimonios = Related('Persona', 'Casado')
    padres = RelatedFrom('Persona', 'Padre')
    hijos = RelatedTo('Persona', 'Padre')

    fecha_entierro = Fecha('fecha_entierro')
    fecha_muerte = Fecha('fecha_muerte')
    edad_deceso = Edad('edad_deceso')
    vivo_en = Fecha("vivo_en")

    fecha_nacimiento = Fecha('fecha_nacimiento')
    fecha_bautizo = Fecha('fecha_bautizo')
    edad_bautizo = Edad('edad_bautizo')

    similares = Related('Persona', 'Similitud')
    matrimonios = Related('Matrimonio', 'Casado')
    parientes = Related('Persona', 'Similitud_Apellidos')

    # Necesito saber si la persona asistió a alguna cosa
    # como padrino o testigo.
    eventos_padrino_matrimonio = RelatedTo("Matrimonio", "Padrino")
    eventos_testigo = RelatedTo("Matrimonio", "Testigo")

    eventos_padrino_bautizo = RelatedTo("Persona", "Padrino")

    @property
    def nombre_completo(self):
        """
        Entrega el nombre completo a partir
        de los campos de Persona.
        """
        return "{0} {1} {2}".format(self.nombre,
                                    self.apellido_paterno,
                                    self.apellido_materno)
    @property
    def apellidos_completos(self):
        """
        Entrega todos los apellidos concatenados
        """
        return "{0} {1}".format(self.apellido_paterno, self.apellido_materno)

    def conyuge(self):
        """
        Entrega el conyuge de la persona (si es que existe)
        """
        for matrimonio in self.matrimonios:
            for pareja in matrimonio.casados:
                if pareja != self:
                    return pareja

    def fechas_eventos_asistidos(self):
        """
        Entrega una lista de todas las fechas de
        eventos a los cuales asistió una persona
        """
        eventos = []
        eventos = eventos + self.fechas_matrimonios_asistidos() + self.fechas_bautizos_asistidos()
        eventos = eventos + self.fechas_nacimiento_hijos()
 
        try:
            if self.vivo_en:
                eventos = eventos.append(self.vivo_en)
        except:
            pass
        return eventos

    def fechas_matrimonios_asistidos(self):
        """
        Lista de todos los matrimonios a los cuales asistió o protagonizó
        esta persona
        """
        eventos = []

        try:
            for matrimonio in self.matrimonios:
                if matrimonio.fecha:
                    eventos.append(matrimonio.fecha)
                elif "Fallecido" in self.conyuge().__ogm__.node.labels():
                    eventos.append(self.conyuge().fecha_entierro)
            for matrimonio in self.eventos_padrino_matrimonio:
                eventos.append(matrimonio.fecha)
            for matrimonio in self.eventos_testigo:
                eventos.append(matrimonio.fecha)
            return eventos
        except:
            return eventos

    def fechas_bautizos_asistidos(self):
        eventos = []
        for evento in self.eventos_padrino_bautizo:
            eventos.append(evento.fecha_bautizo)
        return eventos

    def fechas_nacimiento_hijos(self):
        """
        Obtiene todas las fechas de nacimiento
        de los hijos.
        """
        fechas = []
        for hijo in self.hijos:
            hijo.estimar_nacimiento()
            eventos_hijo = []
            if hijo.fecha_nacimiento:
                fechas.append(hijo.fecha_nacimiento)
            elif hijo.fecha_entierro:
                fechas.append(hijo.fecha_entierro)
            else:
                eventos_hijo = hijo.fechas_eventos_asistidos()
                if(eventos_hijo):
                    for evento in eventos_hijo:
                        if evento:
                            fecha_evento = evento
                            fechas.append(fecha_evento)
        return fechas

    def estimar_nacimiento(self):
        """
        Estima la fecha probable en la cual nació la persona en
        base a las fechas que se tienen disponibles.
        Van en orden de prioridad, de las más seguras a las menos.
        """
        try:
            if self.fecha_bautizo and self.edad_bautizo:
                self.fecha_nacimiento = self.fecha_bautizo - self.edad_bautizo.relativedelta
            elif self.fecha_bautizo:
                self.fecha_nacimiento = self.fecha_bautizo + relativedelta(years=-1)
            elif self.fecha_entierro and self.edad_deceso:
                self.fecha_nacimiento = self.fecha_entierro - self.edad_deceso.relativedelta
        except Exception as e:
            print("Fecha bautizo:{0}".format(self.fecha_bautizo))
            print("Edad bautizo:{0}".format(self.edad_bautizo))
            print("Fecha deceso:{0}".format(self.fecha_entierro))
            print("Edad deceso:{0}".format(self.edad_deceso))
            raise Exception("Hay un problema al encontrar la fecha de nacimiento. Las variables están bien llamadas?")

    def estimar_edad_a_la_fecha(self, fecha: datetime):
        """
        Obtiene la edad que debería tener cierta persona en la fecha que se consulta.
        """
        try:
            return relativedelta(fecha, self.fecha_nacimiento)
        except:
            print('No se encontró fecha de nacimiento')
            raise Exception("No se logró estimar fecha")

    def estimar_apellido_materno(self):
        """
        Si se conoce a la madre se completa el apellido materno faltante.
        """

        if not self.apellido_materno:
            for padre in self.padres:
                if padre.sexo is "Mujer":
                    self.apellido_materno = padre.apellido_materno

class Matrimonio(GraphObject):
    """
    Define un ogm de matrimonio. Une a los casados con padrinos
    y testigos.
    """
    fecha = Fecha('fecha')
    impedimento_consanguineidad = Property('impedimento_consanguineidad')

    casados = Related('Persona', 'Casado')
    padrinos = Related('Persona', 'Padrino')
    testigos = Related('Persona', 'Testigo')
