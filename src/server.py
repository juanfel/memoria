from flask import Flask, render_template, request, redirect, url_for, session
from graph_connector import GraphConnector
from analizador import Analizer, identity_tokenizer
from clustering import Clustering
from flask_wtf import FlaskForm
from wtforms import DecimalField, StringField, FieldList, FormField, Form, SubmitField, BooleanField, IntegerField
from fuzzy import FuzzyFactory
from flask_jsglue import JSGlue
import py_stringmatching as sm
import simplejson as json
import copy

app = Flask(
    __name__, template_folder="./webserver", static_folder="./webserver/js")
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
jsglue = JSGlue(app)

@app.route("/", methods=["GET", "POST"])
def start_page():
    """
    Muestra el index.
    """
    try:
        log = session['log']
    except KeyError:
        log = ""

    graph_form = GraphParametersForm()
    vis_form = VisualizationParametersForm()

    if graph_form.submit_graph_parameters.data and graph_form.validate():
        return redirect(url_for("analize_graph"))

    if request.method == "POST" and vis_form.submit_vis_parameters.data:
        connector = GraphConnector("localhost:7474", "neo4j", "Neo4j")
        print("Mostrando a {0} {1} {2}".format(vis_form.persona1.nombre_re.data,
                                               vis_form.persona1.apellido_paterno_re.data,
                                               vis_form.persona1.apellido_materno_re.data))
        connector.filtrar_personas(vis_form.persona1.nombre_re.data,
                                   vis_form.persona1.apellido_paterno_re.data,
                                   vis_form.persona1.apellido_materno_re.data,
                                   vis_form.generaciones_mat.data,
                                   vis_form.generaciones.data)
        if vis_form.is_shortest.data and vis_form.is_shortest.data != "":
            return redirect(url_for('camino_mas_corto', persona1=json.dumps(vis_form.persona1.data),
                                    persona2=json.dumps(vis_form.persona2.data)))
        return redirect(url_for('print_nodes'))
    return render_template("/index.html", log=log, graph_form=graph_form, vis_form=vis_form)

@app.route("/analize_graph")
def analize_graph():
    """
    Hace el analisis de relaciones.
    """
    try:
        delete_no_score = request.args['delete_no_score']

        if delete_no_score:
            delete_no_score = True
        else:
            delete_no_score = False
    except KeyError:
        print("Usando delete_no_score default")
        delete_no_score = False
    try:
        default_arg = request.args['default']
        if default_arg:
            default = "Conservar"
        else:
            default = "Rechazar"
    except KeyError:
        default = "Rechazar"

    try:
        default_params = json.loads(session['fuzzy_graph_vars'])
    except:
        default_params = FuzzyFactory.default_graph_vars
        print("Usando parametros default")
    analizer = Analizer()
    analizer.analizar_grafo(decision_default=default, eliminar_sin_score=delete_no_score)

    return redirect(url_for('start_page'))

def make_fuzzy_form(defaults=None):
    """
    Crea el formulario para las variables fuzzy.
    Rellena las clases de forms en forma dinámica en base
    a los valores del diccionario.
    """

    class VarsForm(FlaskForm):
        """
        Clase que encapsula a toda la forma del diccionario de
        defaults.
        """
        submit = SubmitField()

    var_types = []

    if defaults == None:
        defaults = copy.deepcopy(FuzzyFactory.default_time_vars)
    for var, prmtrs in defaults.items():
        VarForm = type("VarForm_{0}".format(var), (FlaskForm, ), {})
        for attr_key, val in prmtrs.items():
            if isinstance(val, list):
                print("list")
                setattr(
                    VarForm, attr_key,
                    FieldList(
                        DecimalField(),
                        min_entries=len(val),
                        max_entries=len(val),
                        default=val))
            elif isinstance(val, dict):

                print("Mf")
                MfForm = type("MfForm_{0}_{1}".format(var, attr_key),
                              (FlaskForm, ), {})
                for mf_key, mf_value in val.items():
                    if isinstance(mf_value, list):
                        setattr(
                            MfForm, mf_key,
                            FieldList(
                                DecimalField(),
                                min_entries=len(mf_value),
                                max_entries=len(mf_value),
                                default=mf_value))
                    else:
                        ValueForm = type(
                            "ValueForm_{0}_{1}".format(var, attr_key),
                            (FlaskForm, ), {})
                        for k, v in mf_value.items():
                            setattr(
                                ValueForm,
                                k,
                                DecimalField(default=v))
                            var_types.append(ValueForm)
                        setattr(MfForm, mf_key, FormField(ValueForm))
                        var_types.append(MfForm)
                    setattr(VarForm, attr_key, FormField(MfForm))
            else:
                setattr(VarForm, attr_key, DecimalField(attr_key, default=val))
                var_types.append(VarForm)
        setattr(VarsForm, var, FormField(VarForm))
    return VarsForm, var_types

def remove_keys(obj, rubbish):
    """
    Remueve recursivamente llaves cuyo nombre esté en rubbish.
    Sacado de https://stackoverflow.com/questions/10179033/how-to-recursively-remove-certain-keys-from-a-multi-dimensionaldepth-not-known
    """
    if isinstance(obj, dict):
        obj = {
            key: remove_keys(value, rubbish) 
            for key, value in obj.items()
            if key not in rubbish}
    return obj


@app.route("/time_params", methods=('GET', 'POST'))
def set_fuzzytime_params():
    if(session['fuzzy_time_vars']):
        defaults = json.loads(session['fuzzy_time_vars'])
        defaults = remove_keys(defaults, ["submit"])
    else:
        defaults = None

    form_type, var_types = make_fuzzy_form(defaults=defaults)
    form = form_type()
    if request.method == "POST":

        data = json.dumps(form.data)
        data = remove_keys(json.loads(data), ["csrf_token"])
        session['fuzzy_time_vars'] = json.dumps(data)
        print(session['fuzzy_time_vars'])
        return redirect(url_for("start_page"))
    return render_template("fuzzy.html", form=form, endpoint='set_fuzzytime_params')

@app.route("/graph_params", methods=('GET', 'POST'))
def set_fuzzygraph_params():
    form_type, var_types = make_fuzzy_form(defaults=FuzzyFactory.default_graph_vars)
    form = form_type()
    if request.method == "POST":

        data = json.dumps(form.data)
        data = remove_keys(json.loads(data), ["csrf_token"])
        session['fuzzy_graph_vars'] = json.dumps(data)
        print(session['fuzzy_graph_vars'])
        return redirect(url_for("start_page"))
    return render_template("fuzzy.html", form=form, endpoint='set_fuzzygraph_params')

@app.route("/ruleset")
def print_rule_set():
    """
    Muestra en pantalla las reglas en modo jerarquico
    """
    rule_name = "TiemposCoherentes"
    rule_json = turn_rules_to_json(rule_name)
    return render_template(
        "/rules.html", rule_json=rule_json, rule_name=rule_name)


@app.route("/remake_clusters")
def remake_clusters():
    """
    Recrea a los clusters
    """
    threshold_community = float(request.args['threshold_community'])
    threshold_clustering = float(request.args['threshold_clustering'])
    community_algo = request.args['community_algo']
    clusterizar = request.args['clusterizar']
    connector = GraphConnector("localhost:7474", "neo4j", "Neo4j")
    # Primero hago el cluster inicial y despues lo recalculo
    # en base a las sugerencias de las queries
    connector.do_clustering(threshold=threshold_community, algo=community_algo)
    # connector.do_graph_sim_queries()
    # connector.do_clustering(threshold=threshold_community, algo=community_algo)

    if clusterizar and clusterizar != "False":
        clusterizer = Clustering()
        clusterizer.update_clusters_bd(threshold_clustering, 'inconsistent')

    return redirect(url_for('start_page'))


@app.route("/load_data")
def load_data():
    """
    Recarga los datos
    """
    connector = GraphConnector("localhost:7474", "neo4j", "Neo4j")
    connector.eliminar_db()
    connector.recargar_db()
    return redirect(url_for('start_page'))


@app.route("/analize_names")
def analize_names():
    """
    Analiza los nombres
    TODO: Que muestre un log cuando termine.
    """
    session['log'] = ""
    analizer = Analizer()
    threshold_nombre = float(request.args['threshold_nombres'])
    try:
        analizador = request.args['analizador']
    except KeyError:
        analizador = "OverlapCoefficient"
    analizer_list = {
        "OverlapCoefficient": sm.OverlapCoefficient().get_sim_score,
        "Jaccard": sm.Jaccard().get_sim_score,
        "JaroWinkler": sm.JaroWinkler().get_raw_score,
        "Editex": lambda x, y: max(0, sm.Editex().get_sim_score(x,y))
    }
    try:
        tokenizador = request.args['tokenizador']
    except KeyError:
        tokenizador = "QgramTokenizer"
    tokenizer_list = {
        "Qgram": sm.QgramTokenizer,
        "Alphanumeric": sm.AlphanumericTokenizer,
        "Identity": identity_tokenizer
    }
    log_output = analizer.analizar_nombres(
        threshold_nombre=threshold_nombre,
        opciones_nombre={
            "comparer": analizer_list[analizador],
            "tokenizer": tokenizer_list[tokenizador]
        })
    with open('name_log.txt','w') as log_file:
        log_file.write(log_output)
    session['log'] = log_output
    return redirect(url_for('start_page'))


@app.route("/analize_times")
def analize_times():
    """
    Analiza los nombres
    TODO: Que muestre un log cuando termine.
    """
    analizer = Analizer()
    threshold_nombre = float(request.args['threshold_nombres'])
    threshold_tiempo = float(request.args['threshold_tiempos'])
    try:
        defaults = json.loads(session['fuzzy_time_vars'])
    except:
        print('Usando defaults')
        defaults = FuzzyFactory.default_time_vars

    log_output = analizer.analizar_tiempos(
        threshold_nombre=threshold_nombre, threshold_tiempo=threshold_tiempo, defaults=defaults)
    with open('time_log.txt','w') as log_file:
        log_file.write(log_output)
    session['log'] = log_output
    return redirect(url_for('start_page'))


@app.route("/nodes")
def print_nodes():
    """
    Muestra los nodos y sus relaciones
    """
    threshold_nombre = 0.65
    threshold_tiempo = 0.5
    try:
        show_sim_rels = True if request.args['show_sim_rels'] else False
    except:
        show_sim_rels = False
    try:
        hide_sim_rels = True if request.args['hide_sim_rels'] else False
    except:
        hide_sim_rels = False
    try:
        show_only_parents = True if request.args[
            'show_only_parents'] else False
    except:
        show_only_parents = False
    try:
        threshold_nombre = float(request.args['threshold_nombre'])
    except Exception as e:
        app.logger.info(request.args)
        threshold_nombre = 0.65
    try:
        threshold_tiempo = float(request.args['threshold_tiempo'])
    except Exception as e:
        app.logger.info(request.args)
        threshold_tiempo = 0.5
    node_rels_json = turn_registers_to_json(
        threshold_nombre=threshold_nombre,
        threshold_tiempo=threshold_tiempo,
        no_incluir_similitud=not show_sim_rels,
        esconder_similitud=hide_sim_rels,
        mostrar_solo_padres=show_only_parents)
    return render_template(
        "/nodes.html", rule_json=node_rels_json, rule_name="Nodos")

@app.route("/partition")
def print_nodes_in_partition():
    try:
        partition = int(request.args['partition'])
    except KeyError:
        partition = 0
    connector = GraphConnector("localhost:7474", "neo4j", "Neo4j")
    nodeset = connector.get_nodes_in_partition(partition).data()
    nodeset_json = turn_registers_to_json(no_incluir_similitud=False,
                                          ruleset=nodeset)
    return render_template("nodes.html", rule_json=nodeset_json, rule_name="Nodos", is_partition='true')

@app.route("/shortest_path", methods=["GET", "POST"])
def camino_mas_corto():
    """
    Permite mostrar el camino más corto entre dos personas con nombres conocidos.
    """
    persona1 = json.loads(request.values['persona1'])
    try:
        nombre1 = persona1['nombre_re']
    except KeyError:
        nombre1 = ""
    try:
        a_paterno1 = persona1['apellido_paterno_re']
    except KeyError:
        a_paterno1 = ""
    try:
        a_materno1 = persona1['apellido_materno_re']
    except KeyError:
        a_materno1 = ""

    persona2 = json.loads(request.values['persona2'])
    try:
        nombre2 = persona2['nombre_re']
    except KeyError:
        nombre2 = ""
    try:
        a_paterno2 = persona2['apellido_paterno_re']
    except KeyError:
        a_paterno2 = ""
    try:
        a_materno2 = persona2['apellido_materno_re']
    except KeyError:
        a_materno2 = ""

    connector = GraphConnector("localhost:7474", "neo4j", "Neo4j")
    nodeset = connector.camino_mas_corto(nombre1, a_paterno1, a_materno1,
                                         nombre2, a_paterno2, a_materno2).data()
    print("{0}, {1}, {2}".format(nombre1, a_paterno1, a_materno1))
    print("{0}, {1}, {2}".format(nombre2, a_paterno2, a_materno2))
    nodeset_json = turn_registers_to_json(ruleset=nodeset)
    return render_template("nodes.html", rule_json=nodeset_json, rule_name="Camino mas corto", is_partition="false")


def turn_rules_to_json(rule_name):
    """
    Transforma los resultados de neo4j a un json entendible por 
    vis.js
    """
    nodes = []
    rels = []
    node_label_keys = ['variable', 'consecuente']

    connector = GraphConnector("localhost:7474", "neo4j", "Neo4j")
    ruleset = connector.get_rules_as_node_rels(rule_name).data()

    # Hago la distinción entre los nodos que van al json y los que son
    # del grafo en si mismo
    graph_nodes = ruleset[0]['nodes']
    graph_rels = ruleset[0]['rels']

    for node in graph_nodes:
        node_dict = {}
        node_dict['id'] = id(node)
        for label in node_label_keys:
            if label in node.keys():
                node_dict['label'] = node[label]
        node_dict['properties'] = dict(node)
        nodes.append(node_dict)
    for rel in graph_rels:
        rel_dict = {}
        rel_dict['id'] = id(rel)
        rel_dict['from'] = id(rel.start_node)
        rel_dict['to'] = id(rel.end_node)
        rel_dict['label'] = rel['funcion_membresia']
        rel_dict['properties'] = dict(rel)
        rels.append(rel_dict)

    end_dict = {"nodes": nodes, "edges": rels}

    return json.dumps(end_dict)


def turn_registers_to_json(threshold_nombre=0.65,
                           threshold_tiempo=0.5,
                           no_incluir_similitud=True,
                           esconder_similitud=False,
                           mostrar_solo_padres=False,
                           ruleset=None):
    """
    Obtiene los registros con todas sus relaciones y obtiene un json que lo puede
    usar vis.js
    """
    nodes = []
    rels = []
    rel_label_keys = ["Similitud", "Casado", "Padre", "Padrino", "Testigo"]

    if ruleset is None:
        connector = GraphConnector("localhost:7474", "neo4j", "Neo4j")
        ruleset = connector.get_all_rels(threshold_nombre, threshold_tiempo).data()

    # Hago la distinción entre los nodos que van al json y los que son
    # del grafo en si mismo
    graph_nodes = ruleset[0]['nodes']
    graph_rels = ruleset[0]['rels']

    for node in graph_nodes:
        node_dict = {}
        node_dict['id'] = id(node)
        if "Persona" in node.labels or "Persona_Virtual" in node.labels:
            node_dict['label'] = "{0} {1} {2}".format(node['nombre'],
                                                      node['apellido_paterno'],
                                                      node['apellido_materno'])
        else:
            if mostrar_solo_padres:
                continue
            node_dict['label'] = str(node.labels)
        node_dict['properties'] = dict(node)
        nodes.append(node_dict)
    for rel in graph_rels:
        if 'Similitud' in rel.types() and no_incluir_similitud:
            continue
        if "Padre" not in rel.types() and mostrar_solo_padres:
            continue
        rel_dict = {}
        rel_dict['id'] = id(rel)
        rel_dict['from'] = id(rel.start_node)
        rel_dict['to'] = id(rel.end_node)
        for label in rel_label_keys:
            if label in rel.types():
                rel_dict['label_hidden'] = label
                if label is "Similitud":
                    rel_dict[
                        'value'] = rel['similitud_nombre'] + rel['coherencia_temporal']
                    if esconder_similitud:
                        rel_dict['hidden'] = 'true'
                break
        rel_dict['properties'] = dict(rel)
        rels.append(rel_dict)

    end_dict = {"nodes": nodes, "edges": rels}
    return json.dumps(end_dict)


class GraphParametersForm(FlaskForm):
    default = BooleanField(label="Aceptar inciertos por default", default="Checked")
    delete_no_score = BooleanField(label="Eliminar sin puntaje de grafo", default="Checked")
    submit_graph_parameters = SubmitField()

class NameRegexForm(FlaskForm):
    nombre_re = StringField(label="Nombre (regex)", default=".*")
    apellido_paterno_re = StringField(label="Apellido Paterno (regex)", default=".*")
    apellido_materno_re = StringField(label="Apellido Materno (regex)", default=".*")

class VisualizationParametersForm(FlaskForm):
    persona1 = FormField(NameRegexForm)
    generaciones_mat = IntegerField(label= "Numero de Generaciones con Matrimonios", default=0)
    generaciones = IntegerField(label= "Numero de Generaciones sin Matrimonios", default=6)
    is_shortest = BooleanField(label="Buscar camino mas corto entre dos personas")
    persona2 = FormField(NameRegexForm)
    submit_vis_parameters = SubmitField()


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
