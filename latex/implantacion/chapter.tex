\chapter{Implantación y Validación de la Solución}%
\label{cha:implantación_y_validación_de_la_solución}

En el capitulo presente se probará el sistema usando datos genealógicos
obtenidos desde registros eclesiásticos de los siglos XVIII y XIX\@. Las pruebas
consistirán en la ejecución de los algoritmos usando distintos parámetros,
obteniendo diferentes árboles genealógicos de personas consideradas
interesantes. Se analizarán comparando los resultados con árboles encontrados
manualmente.

\section{Hardware}%
\label{sec:hardware}

Las siguientes pruebas se realizaron sobre un computador de escritorio cuyas características son:
\begin{itemize}

    \item \textbf{CPU:} \textit{AMD Ryzen 5 1400 Quad-Core @ 8x 2.744GHz}  

    \item \textbf{RAM:} \textit{4 GB} 

    \item \textbf{OS:} \textit{Arch Linux}, \textit{kernel x86\_64 Linux 4.19.4-arch1-1-ARCH} 

    \item \textbf{Versión de \textit{Neo4j}:} 3.4.9 ejecutado desde \textit{docker}. 

\end{itemize}

\section{Conjunto de datos}%
\label{sec:conjunto_de_datos}

Se trabaja sobre un conjunto de datos que consiste en 944 registros eclesiásticos de distintos tipos.
Se aprecia una cantidad considerablemente mayor de matrimonios en comparación a
otros tipos de eventos. Esto puede causar problemas al momento de calcular cotas inferiores en las fechas de nacimiento.
Si se compara la cantidad de padres y madres se puede deducir
que hay madres solteras dentro de los datos, ya que la cantidad de madres es mayor a la de padres. Sin embargo
en total hay más hombres que mujeres. 
El desglose se presenta en las tablas~\ref{tab:desglose1},~\ref{tab:desglose2}~y~\ref{tab:desgloce3}. 

\begin{table}[!htpb]
    \centering
    \caption{Desglose de registros dentro del \textit{dataset} de acuerdo al tipo de evento}
    \label{tab:desglose1}
    \begin{tabular}{c c c c}
        \toprule
     Defunciones & Matrimonios & Bautizos & Total \\\toprule
     121    & 738 & 57 & 944 \\\bottomrule
    \end{tabular}
\end{table}

\begin{table}[!htpb]
    \centering
    \caption{Desglose de registros dentro del \textit{dataset} de acuerdo al tipo de persona}
    \label{tab:desglose2}
    \begin{tabular}{c c c c c c c c}
    \toprule
    Padre & Madre & Casado & Padrino & Testigo & Bautizado & Fallecidos & Total \\\toprule
    234 & 262 & 234 & 65 & 71 & 19 & 48 & 944 \\\bottomrule
    \end{tabular}
\end{table}
\begin{table}[!htpb]
    \centering
    \caption{Desglose de registros de acuerdo al sexo de la persona}
    \label{tab:desgloce3}
    \begin{tabular}{c c c c}
        \toprule
    Hombre & Mujer & No se sabe & Total \\\toprule
    482 & 451 & 11 &  944 \\\bottomrule
    \end{tabular}
\end{table}

\section{Metodología de las pruebas}%
\label{sec:Metodología de las pruebas}

Se ejecutará el algoritmo completo usando una configuración en específico que
consisten las opciones relevantes de nombre (ej: elección del algoritmo de
comparación de \textit{strings}), tiempo y grafo, más los parámetros de las
variables difusas cuando hayan cambios importantes con respecto a su
\textit{default}. Luego se unirán usando \textit{Louvain} para la detección de
personas. Finalmente se buscarán y comentarán los árboles resultantes de las
siguientes personas:

\begin{itemize}

    \item Juan Antonio Duval: el árbol genealógico de es de tamaño pequeño. Presenta problemas de ambigüedad ya
        que hay tres generaciones de personas que tienen el mismo nombre (Juan Antonio) y no se tiene la suficiente
        información para saber quien es padre de quien.

    \item Pascual Basoalto: su familia es de tamaño grande en comparación al anterior. Permite mostrar la capacidad
        del sistema de armar árboles con múltiples generaciones. Por su tamaño la posibilidad de encontrar anomalías
        al momento de cambiar parámetros es más grande que con la persona anterior.

\end{itemize}

\section{Primera prueba: Parámetros por defecto}%
\label{sec:prueba_numero_1}
Primero se realiza la ejecución del programa usando los parámetros predefinidos, los cuales consisten en:
\begin{itemize}
    \item \textbf{Nombres}:
        \begin{itemize}
            \item Parámetros \textit{fuzzy}: por defecto (explicados en sección~\ref{ssub:controlador_difuso_nombres})
            \item Coeficiente: \textit{OverlapCoeficient}
            \item Tokenizador: \textit{por q-gramas}
            \item \textit{Threshold}: 0.65
        \end{itemize}
    \item \textbf{Tiempos}: 
        \begin{itemize}
            \item Parámetros \textit{fuzzy}: por defecto (explicados en sección~\ref{semejanza_por_tiempos})
        \end{itemize}
    \item \textbf{Grafos}:
        \begin{itemize}
            \item Parámetros \textit{fuzzy}: por defecto (explicados en sección~\ref{sub:semejanza_por_relaciones_de_grafo})
            \item Eliminar sin puntaje: verdadero
            \item Aceptar inciertos: verdadero
        \end{itemize}
\end{itemize}

\subsection{Pascual Basoalto}%
\label{sub:pascual_basoalto1}

Al buscar por ``Pascual Basoalto'' se encuentran dos árboles
(Figura~\ref{fig:implantacion/prueba1_pascual_general}); el más pequeño de ellos
(Figura~\ref{fig:implantacion/prueba1_pascual_barbara}) consiste en solo dos
integrantes: Pascual, y su madre Bárbara. Se puede asumir que se trata de una
madre soltera, considerando que el hijo tiene su apellido.

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{implantacion/prueba1_pascual_general.png}
    \caption{Vista general de los arboles genealógicos que contienen a Pascual Basoalto.}
    \label{fig:implantacion/prueba1_pascual_general}
\end{figure}

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.1\linewidth]{implantacion/prueba1_pascual_barbara.png}
    \caption{Familia de Pascual y Bárbara Basoalto.}
    \label{fig:implantacion/prueba1_pascual_barbara}
\end{figure}

La rama más grande consiste en cinco generaciones de Basoalto
(Figura~\ref{fig:implantacion/prueba1_pascual_centrado}). Pascual Basoalto es
hijo de Francisca (Díaz de) León y Francisco Basoalto. El caso de la madre de
Pascual es interesante porque es un ejemplo donde hay que usar ``apodos''.
Normalmente la similitud entre ``Díaz de León'' y ``León'' es prácticamente
nula, pero diciéndole al sistema que ambos son equivalentes permite asegurar
que exista una comparación entre ellos.

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{implantacion/prueba1_pascual_centrado.png}
    \caption{Árbol genealógico centrado en Pascual Basoalto León.}
    \label{fig:implantacion/prueba1_pascual_centrado}
\end{figure}

La partición de Pascual Basoalto León es un ejemplo de registros íntimamente
interconectados (Figura~\ref{fig:implantacion/prueba1_pascual_particion}). Es
prácticamente como se debería ver una partición después de la poda, donde todos
se parecen entre sí, dando evidencia de que los registros deben ser unidos. La
excepción está en un registro que solo se parece a uno de sus pares; en este
caso es porque se parecen los padres. Con el resto se presume que no está
conectado porque el resto se parece entre sí por sus hijos y matrimonios. Este
caso puede ser evidencia de que la restricción, donde solo se conservan aquellos
que tengan puntaje de grafo, es muy restrictiva para este caso.

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{implantacion/prueba1_pascual_particion.png}
    \caption{Partición de Pascual Basoalto León}
    \label{fig:implantacion/prueba1_pascual_particion}
\end{figure}

\subsection{Juan Antonio Duval}%
\label{sub:juan_antonio_duval}

Cuando se busca por ``Juan Antonio Duval'' se encuentran tres
familias (Figuras~\ref{fig:implantacion/prueba1_jduvalsalazar}~y~\ref{fig:implantacion/prueba1_jduvalcanalesbernal}).
Todas ellas tienen un patriarca con el nombre buscado y un hijo con el mismo
nombre.  El hecho de que ambos compartan el nombre puede generar ambigüedades,
cosa que no ocurrió en este \textit{test} ya que se fue estricto en que se compararan
solo aquellos en los cuales se está seguro de que hay información de grafo. 

Es probable que haya algún nexo entre estas tres ramas, ya que vivieron
en fechas relativamente comparables. \textit{A priori} se sabe que uno
de ellos es padre del otro, pero para un humano la tarea de saber cómo es realmente
la relación entre ellos es muy complicada.

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{implantacion/prueba1_jduvalsalazar.png}
    \caption{Árbol que contiene a Juan Duval Salazar.}
    \label{fig:implantacion/prueba1_jduvalsalazar}
\end{figure}

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{implantacion/prueba1_jduvalcanalesbernal.png}
    \caption{Árboles que contienen a Juan Duval Canales y a Juan Duval Bernal.}
    \label{fig:implantacion/prueba1_jduvalcanalesbernal}
\end{figure}

\section{Segunda prueba: No eliminar sin puntaje de grafo}%
\label{sec:segunda_prueba_eliminar_sin_puntaje_de_grafo}

En esta prueba se quiere verificar el efecto que tiene el no eliminar aquellas similitudes
donde no se pudieron comparar por grafo. \textit{A priori} se puede esperar que se mantengan
más relaciones de similitud y, por lo tanto, se unan más personas.

\subsection{Pascual Basoalto}%
\label{sub:pascual_basoalto}

Al buscar por ``Pascual Basoalto'' se genera un árbol extremadamente complicado
como para corresponder a la realidad
(Figura~\ref{fig:implantacion/prueba2_pascual_basoalto_general}). Si se mira a
los ascendientes y descendientes directos de ``Pascual Basoalto León''
(Figura~\ref{fig:implantacion/prueba2_pascual_basoalto_centrado}) se puede
apreciar la presencia de una hija de ``Pascual Basoalto'' y ``Francisca León'', lo
cual es incorrecto en este caso.

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{implantacion/prueba2_pascual_basoalto_general.png}
    \caption{Vista general del árbol genealógico de Pascual Basoalto.}
    \label{fig:implantacion/prueba2_pascual_basoalto_general}
\end{figure}

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{implantacion/prueba2_pascual_basoalto_centrado.png}
    \caption{Descendientes y ascendientes directos de Pascual Basoalto León.}
    \label{fig:implantacion/prueba2_pascual_basoalto_centrado}
\end{figure}

Otro caso anómalo es el de José María Basoalto
(Figura~\ref{fig:implantacion/prueba2_jose_basoalto}), el cual tiene múltiples
padres y madres. Probablemente al relajar la condición de que solo se mantienen
aquellos comparados por grafo se conservan relaciones superfluas que no aportan
en nada.

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{implantacion/prueba2_jose_basoalto.png}
    \caption{Árbol genealógico de José María Basoalto.}
    \label{fig:implantacion/prueba2_jose_basoalto}
\end{figure}

\subsection{Juan Antonio Duval}%
\label{sub:juan_antonio_duval}

Cuando se busca por ``Juan Antonio Duval'' se encuentra un grafo un poco más
compacto que en la primera prueba (Figura~\ref{fig:implantacion/prueba2_jduval}). En
este caso sí se considera que hay ambigüedades al momento de decidir que cual Juan Antonio Duval es cual, ya
que se sabe que en ese grupo están representados tanto los padres como
los hijos (ya que hay una flecha que apunta a si mismo), pero no hay suficiente información como para saber
quien es quien. A lo menos confirma que ocurrieron en tiempos relativamente comparables. También se puede confirmar
que Juan Duval, padre de Juan Antonio Duval Salazar, es el más antiguo de ellos (Magdalena Salazar es la madre más antigua del
grupo). Por lo tanto Juan Antonio Duval Salazar puede que se haya casado con alguien de su misma generación. 

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{implantacion/prueba2_jduval.png}
    \caption{Árbol genealógico de Juan Antonio Duval.}
    \label{fig:implantacion/prueba2_jduval}
\end{figure}

Dado los casos anteriores se asume que es necesario eliminar aquellos que no se
pudieron comparar mediante relaciones de grafo si se quieren resultados más
certeros. Mantener relaciones sin comparación de grafo puede ser útil en casos
donde se sabe que hay ambigüedades que se puedan hacer más evidentes
conservando algunas de las relaciones sin evidencia por árbol (resultado de
comparar actas de nacimientos con el matrimonio de un hijo, por ejemplo).

\section{Tercera Prueba: Usando \textit{Jaro-Winkler} como comparación de \textit{Strings}}%
\label{sec:tercera_prueba_usando_jaro-winkler_como_comparacion_de_strings}

En esta prueba se quiere saber el efecto que tiene cambiar el cálculo de puntajes de nombres desde
uno basado en \textit{tokens} a uno basado en \textit{distancia de edición}. Se usó \textit{Jaro-Winkler} 
donde el puntaje necesario para ser guardado es de $0.7$, indicando que el parecido entre dos
nombres debe ser de al menos $0.7$, sino no se guarda. Después se comparará con el mismo algoritmo
pero con un \textit{threshold} de $0.8$.

\subsection{Pascual Basoalto con un \textit{threshold} de $0.7$}%
\label{sub:pascual_basoalto_con_un_threshold_de_0_7_}

Se puede observar que al buscar por ``Pascual Basoalto'' se obtiene una familia con más conexiones
(Figura~\ref{fig:implantacion/prueba3_Basoalto_General}). Al centrarse en Pascual Basoalto León
se puede corroborar que sus familiares se mantienen intactos en comparación con la primera prueba (Figura~\ref{fig:implantacion/prueba1_pascual_general}).
\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.4\linewidth]{implantacion/prueba3_Basoalto_General.png}
    \caption{Vista general de la familia Basoalto.}
    \label{fig:implantacion/prueba3_Basoalto_General}
\end{figure}

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.4\linewidth]{implantacion/prueba3_Basoalto_Pascual.png}
    \caption{Vista a la familia de Pascual Basoalto León.}
    \label{fig:implantacion/prueba3_Basoalto_Pascual}
\end{figure}

Con Pablo Basoalto se genera una anomalía, ya que tiene muchos padres
(Figura~\ref{fig:implantacion/prueba3_Basoalto_Pablo}). Como el sistema tolera
múltiples cambios a los nombres entonces hace que se generen similitudes con
personas con las cuales son bastante diferentes (Figura~\ref{fig:implantacion/prueba3_Basoalto_Pablo_Particion}).

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.4\linewidth]{implantacion/prueba3_Basoalto_Pablo.png}
    \caption{Vista a Pablo Basoalto.}
    \label{fig:implantacion/prueba3_Basoalto_Pablo}
\end{figure}

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.8\linewidth]{implantacion/prueba3_Basoalto_Pablo_Particion.png}
    \caption{Vista a la partición de Pablo Basoalto.}
    \label{fig:implantacion/prueba3_Basoalto_Pablo_Particion}
\end{figure}

\subsection{Juan Antonio Duval con un \textit{threshold} de 0.7}%
\label{sub:juan_antonio_duval_con_un_threshold_de_0_7}

En este caso también se generan conexiones extra, generadas por relajar los requisitos
para aceptar un puntaje de nombre (Figura~\ref{fig:implantacion/prueba3_Duval}). 
Se puede apreciar que el problema de la ambigüedad
en la ascendencia de Juan Antonio Duval es solucionado haciendo que uno de ellos
tenga hijos con Magdalena Salazar y Mariana Canales a la vez, insinuando la presencia
de un segundo matrimonio.

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.4\linewidth]{implantacion/prueba3_Duval.png}
    \caption{Vista a Juan Antonio Duval.}
    \label{fig:implantacion/prueba3_Duval}
\end{figure}

\subsection{\textit{Threshold} en 0.8}%
\label{sub:threshold_en_0_8}
Con un puntaje mínimo de 0.8 se obtienen resultados similares a la primera prueba tanto para
``Pascual Basoalto'' como para ``Juan Antonio Duval'' (Figura~\ref{fig:prueba3_1}). Esto implica que el parámetro
\textit{threshold} debería tener un valor entre $0.7$ y $0.8$ para lograr resultados 
pasables. Probablemente exista un valor óptimo que permita adquirir más información sin
introducir ruido en dicho rango. Desafortunadamente, con el hardware usado cada prueba
toma cerca de 40 minutos lo cual hace que buscar exhaustivamente un resultado positivo
tome mucho tiempo.

\begin{figure}[!htpb]
    \centering
    \begin{subfigure}[!htpb]{0.3\textwidth}
        \centering
        \includegraphics[width=\linewidth]{implantacion/prueba_3_1_Duval.png}
        \caption{Juan Antonio Duval}
    \end{subfigure}
    \begin{subfigure}[!htpb]{0.3\textwidth}
        \centering
        \includegraphics[width=\linewidth]{implantacion/prueba3_1_Basoalto.png}
        \caption{Pascual Basoalto}
    \end{subfigure}
    \caption{Resultados de usar un \textit{threshold} de 0.8}
    \label{fig:prueba3_1}
\end{figure}
\section{Prueba cuatro: Cambios a rangos de tiempo (\textit{DEV})}%
\label{sec:prueba_cuatro}

En esta prueba se cambio la variable \textit{DEV} usando como parámetros $[a:50, b.120]$, lo que implica que 
considera hasta unos 50 años para que la \textit{distancia entre eventos} tenga como resultado \textit{normal}, bajando
gradualmente a \textit{lejos} desde 50 años a 120 años. Esto amplía el rango donde la distancia entre
eventos es normal. 

Se escoge \textit{DEV} ya que existe una gran cantidad de registros que no son ``primarios'' (personajes centrales
de un acta de nacimiento, matrimonio o defunción) y por lo tanto no son capturados por el resto
de las variables.

Haciendo este cambio solo se encontró una diferencia significativa al buscar por ``Juan Antonio Duval'', por
lo cual solo se muestran sus resultados (Figura~\ref{fig:implantacion/prueba4_DEV}). En dicho caso aparece un
hijo nuevo, \textit{Pedro Ubal}, el cuál es hijo de un Juan Ubal pero de madre desconocida. Lo anterior puede
implicar la presencia de otro matrimonio, o que simplemente hacen falta más datos para poder probar
o desmentir que Pedro Ubal fue hijo de Juan Duval y que simplemente hubo un cambio de apellido por parte del
sacerdote a cargo.

\begin{figure}[!htpb]
    \centering
    \includegraphics[width=0.4\linewidth]{implantacion/prueba4_DEV.png}
    \caption{Árbol genealógico de Juan Antonio Duval}
    \label{fig:implantacion/prueba4_DEV}
\end{figure}

Se puede concluir que aumentando el rango donde \textit{DEV} es considerado \textit{normal} efectivamente
se conservan más registros como \textit{similares}.

\section{Rendimiento de la base de datos}%
\label{sec:rendimiento_de_la_base_de_datos}

Hay que mencionar que la ejecución completa de los pasos del algoritmo toma un tiempo de aproximadamente
40 minutos, siendo el paso de comparar nombres el que toma más tiempo. La culpa en este caso
consiste en que es necesario calcular la similitud de nombres entre todos los pares de
personas existentes, lo cual es costoso. 

El tiempo hipotéticamente se podría mejorar aplicando alguna estrategia de
paralelismo, pero en la práctica se hace complicado porque \textit{Py2neo} no
soporta el uso de
\textit{multiprocess}~\cite{technige_deadlock_nodate}~\cite{technige_using_nodate}. Además \textit{multithreading}
no sirve en este caso, ya que el problema de rendimiento está dado por la \textit{CPU} y \textit{Cpython} solo
ejecuta un thread a la vez~\cite{python_software_foundation_threading_nodate}.

Al probar el programa en el \textit{notebook} de desarrollo, el cual tiene como
especificaciones una \textit{CPU AMD A8-3510MX APU} y \textit{8 GB} de
\textit{RAM}, se puede notar una grave ralentización del programa, subiendo el
tiempo de ejecución a más de 12 horas. 

Otra opción para reducir los tiempos es limitar la comparación a nombres o apellidos iguales. De esa forma solo
se comparan aquellos que tengan el mismo nombre pero sin necesariamente saber si los apellidos
son similares, o aquellos que tienen los mismos apellidos, asegurando que aquellos que sean
comparados sean de la misma familia pero que tengan distintas representaciones en su nombre. 
De ésta forma se consiguen resultados en 5 minutos en el computador de prueba. Sin embargo
no está exento de consecuencias la realización de dicha simplificación, la cual incluye la reducción
de posibles similitudes de nombres.

Un efecto importante de hacer aún más estricta la selección por nombres es que
la estrategia de usar \textit{apodos} no funciona tan bien, ya que si no son el
mismo apellido y nombre entonces no hay comparación, por lo tanto no se puede aplicar el apodo. Con
los sujetos de prueba usados no ocurrió este suceso, pero puede ser una consideración
importante con \textit{datasets} mas grandes y/o con más errores tipográficos.


\section{Resumen de los resultados}%
\label{sec:resumen_de_los_resultados}

Los resultados obtenidos en esta sección en general son alentadores. Para casos simples logra conectar
los distintos individuos encontrando árboles genealógicos que tienen sentido. En casos más complicados
un genealogista tiene la posibilidad de investigar diversas formas de atacar el problema de la ambigüedad
presente mediante el ajuste de parámetros del algoritmo. Esto necesariamente implica
relajar algunas condiciones, tales como hacer que la comparación de nombres exija una menor similitud para
considerarlos como posibles candidatos a ser las mismas personas, o hacer que el sistema use un rango
de tiempo mayor para buscar candidatos; resultando en una mayor posibilidad de encontrar inconsistencias en los
árboles. 

Desafortunadamente el rendimiento del sistema podría ser mejor, ya que si bien la simplificación
usada acelera notablemente la comparación entre los diferentes registros, es posible que
el tiempo de ejecución siga siendo alto cuando la cantidad de datos llegue a los millones. Las formas de mejorar
esto no solo pasan por buscar más esquemas de \textit{blocking} o de usar \textit{hardware} más
poderoso, sino que también implica portar el sistema a algún entorno que permita usar los múltiples núcleos
de un procesador en una forma eficiente.

