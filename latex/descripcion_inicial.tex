\documentclass[letterpaper,12pt]{article}
\usepackage{amsmath}
\usepackage{fontspec}
\usepackage{xunicode}
\usepackage[spanish, es-tabla]{babel}
\usepackage[letterpaper, includeheadfoot, margin=2.5cm]{geometry}
\usepackage{float}
\usepackage[numbers, sort&compress]{natbib}
\usepackage{url}
\author{Juan Felipe Avalo}
\title{Descripción Inicial}
\date{}

\begin{document}
\maketitle

\section{Objetivos}
\label{sec:objetivos}
\subsection{Objetivos Generales}
\begin{itemize}
    \item Implementar un sistema de armado y análisis de árboles genealógicos a partir de 
        datos no limpios y duplicados usando Bases de Datos de Grafos para el análisis eficiente y efectivo de información genealógica.
\end{itemize}
\subsection{Objetivos Específicos}
\begin{itemize}
    \item Comparar distintas alternativas de algoritmos para la deduplicación efectiva de datos genealógicos representados mediante grafos.
    \item Analizar los datos obtenidos mediante la generación de reportes para encontrar información genealógica novedosa.
    \item Verificar empíricamente el rendimiento de una Base de Datos de Grafos, determinando si su uso
        es factible para un conjunto de datos no limpios y duplicados.
\end{itemize}


\section{Definición inicial del problema}
\label{sec:definicion_inicial_del_problema} 

La genealogía se encarga de estudiar la ascendencia y descendencia de personas
o familias, organizando sus resultados en árboles genealógicos. \\

Las fuentes de datos pueden ser de distinta procedencia. En particular,
faltando datos del registro civil, se hace necesario recurrir a información
proveniente de los registros de las iglesias donde guardaban las actas de
matrimonio, defunción, bautizos, entre otros.\\


Por otro lado, una vez armado el árbol podrían quedar cabos sueltos. Si, por
ejemplo, de la madre de una persona solo se conoce su nombre ¿Puede ser posible
encontrar si es miembro de otra rama de la familia?.
Un genealogista tendría que hacer uso de su juicio para resolver esas
preguntas, revisando todos los registros tratando de buscar alguna posible
relación con otra persona, lo cual es una tarea tediosa.\\

Con el uso de una base de datos de grafos la búsqueda de relaciones se puede
simplificar exponencialmente, permitiendo al genealogista buscar información de
padres, abuelos, o primos en tercer grado, por ejemplo. \\

Desafortunadamente, considerando lo heterogéneas que son las fuentes de datos
hay que tener un cierto grado de certeza al momento de declarar si una persona
de un registro es la misma que la de otro registro completamente distinto.\\

Para esto, es deseable encontrar formas de automatizar la búsqueda de similitudes entre las
distintas entidades descritas en los registros. De esa manera al
momento de revisar posibles nuevos miembros de la familia el genealogista podrá
ver aquellos que tengan mayor probabilidad de ser familiares, y que rol
cumplían dentro de ellos.\\

Una vez formadas las conexiones entre los posibles miembros de las familias se debe poder encontrar
relaciones que normalmente están ocultas al ojo humano, como por ejemplo poder ver si es que
hay parentesco con alguna persona especificada, o si hay algún patron en el movimiento de 
los distintos miembros de la familia, entre otros análisis que son viables de realizar mediante
un modelo de grafos.\\

Últimamente, el resultado esperado es tener una herramienta que permita al
genealogista analizar datos demográficos de personas para obtener información de
interés en forma sencilla.

\section{Discusión bibliográfica preliminar}
\label{sec:discusion_bibliografica_preliminar}

Los datos de un árbol genealógico se pueden modelar naturalmente como nodos y
relaciones. Las bases de datos relacionales y NoSQL de tipo llave-valor, de
columnas o de documentos modelan relaciones de tipo ad-hoc en forma
deficiente~\cite{robinson_graph_2013}. En el caso de SQL se agrega
complejidad al momento de unir distintas tablas, aumentándose la complejidad al
momento de hacer consultas de distintas fuentes. Además para hacer consultas
recíprocas (Por Ejemplo: Si A es amigo de B, ¿es B amigo de A?) se incurre en
un costo computacional elevado. El mismo caso se da en los tipos de bases de datos
NoSQL mencionadas anteriormente~\cite{robinson_graph_2013}. \\

\textit{Kirby et al.} compararon el rendimiento entre \textit{MariaDB} y
\textit{Neo4j}, una base de datos relacional y de grafos respectivamente para
un \textit{dataset} de nacimientos, muertes y
matrimonios~\cite{kirby_comparing_nodate}. La primera diferencia es que la
misma consulta en \textit{MariaDB} es mucho mas larga en lineas de código que
Neo4j, mostrando una diferencia en la capacidad de expresión de los lenguajes
de dichas bases de datos.  La segunda diferencia es que el tiempo que demoró
\textit{Neo4j} en completar sus consultas fue inferior a \textit{MariaDB}. \\

En~\cite{lundberg_fuzzy_2015} se implementó la unión de árboles familiares
mediante \textit{fuzzy matching} y \textit{Neo4j}. Dicho problema es
tangencialmente relacionado al problema tratado por la memoria, ya que la
genealogías a unir ya están armadas, lo cual no es necesariamente el caso del
trabajo a realizar. Lo valioso está en mostrar una aplicación de comparaciones
difusas entre los distintos atributos que componen el registro de una persona.
\\

Existen diversas formas de comparar registros. Por ejemplo, para cadenas de
caracteres como el nombre de una persona se puede usar la \textit{distancia de
Levenshtein}. Consiste en ver cuantas inserciones, eliminaciones o reemplazos
hay entre los dos nombres. Es especialmente útil cuando hay errores
tipográficos.~\cite{navarro_guided_nodate} \\

Otra forma de comparar nombres es comparándolos mediante \textit{distancia
coseno}~\cite{perone_machine_nodate}. Implica transformar los textos a un
espacio vectorial y compararlos mediante su producto punto, bajo la idea de que
ambos nombres deberían apuntar en la misma dirección.\\

La \textit{lógica difusa} sirve cuando se desea tratar con un problema donde no
se tienen datos precisos y certeros~\cite{wierman_introduction_2010}. En
genealogía puede ser útil trabajar con reglas donde no se indique con precisión
como se quiere que se clasifique algo. Por ejemplo, en vez de decir "Dos
personas son similares si nacieron con a lo más 10 años de diferencia", se
puede expresar de la siguiente forma: "Dos personas son similares si nacieron
con una fecha CERCANA", donde CERCANA es una regla difusa.\\

Existen formas de comparación que son útiles con datos que tienen relaciones.
En~\cite{weis_relationship-based_2006} se comparan conjuntos de autores y
películas, buscando duplicados. Los nombres de cada uno de ellos pueden tener
errores, pero por como se relacionan entre ellos es posible inferir cuando son
iguales. Si bien éste caso es usado en datos con formato xml, dado que
transforman los datos a un modelo de grafos es posible usar dichos algoritmos
para genealogía.

\section{Plan de trabajo} \label{sec:plan_de_trabajo}
\begin{table}[H]
    \centering
    \caption{Plan de Trabajo}
    \label{tab:plan_de_trabajo}
    \begin{tabular}{|c|c|}
        \hline
        Tarea & Tiempo de duración \\
        \hline
        Ordenar y limpiar datos & 1 semana \\
        Establecer campos del modelo de datos & 2 semanas\\
        Ingresar datos a la base de datos & 2 semanas\\
        Encontrar relaciones que permitan unir los registros & 2 semanas\\
        Implementar la unión de los registros & 1 mes\\
        Implementar la generación de árboles genealógicos a partir de los registros & 2 semanas\\
        Comparar árboles generados con los reales & 2 semanas\\
        Búsqueda de consultas de interés & 2 semanas\\
        Implementación de consultas de interés & 4 semanas\\
        Análisis de resultados de consultas & 2 semanas\\\hline
    \end{tabular}
\end{table}
\bibliographystyle{unsrtnat}
\bibliography{bibliografia.bib}
\end{document}
