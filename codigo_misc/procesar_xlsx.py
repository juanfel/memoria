#!/usr/bin/python
"""
Crea los csv para trabajarlos despues.
"""
from os import listdir, makedirs
import subprocess
import argparse
import re
import shlex

SHEET_TIPES = ['BAUT', 'MAT', 'DEF']


def convertir_archivos(archivos, output, carpeta):
    """Convierte una lista de archivos .xlsx a .csv.

    :archivos: Lista de nombres de archivo .xlsx
    :output: Lista de nombres de archivo .csv de salida
    :returns: TODO

    """
    tuplas = list(zip(archivos, output))
    for tipo in SHEET_TIPES:
        print("Procesando " + tipo)
        for archivo, output in tuplas:
            print("Archivo: " + carpeta + "/" + archivo)
            subprocess.run(['xlsx2csv', '-i', '-n' + tipo,
                            carpeta + "/" + archivo,
                            carpeta + "/" + tipo + "/" + output])


def crear_carpetas(carpeta, SHEET_TIPES):
    """Crea las carpetas necesarias para guardar cada
    tipo de sheet distinto.

    :carpeta: String con la carpeta a crear
    :SHEET_TIPES: TODO
    :returns: TODO

    """
    for tipo in SHEET_TIPES:
        makedirs(carpeta + "/" + tipo, exist_ok=True)


def unir_csv(carpeta, SHEET_TIPES):
    """Une el contenido de todos los csv en uno solo
    final por tipo de archivo.

    :carpeta: String con la carpeta a crear
    :SHEET_TIPES: TODO
    :returns: TODO

    """
    regex = re.compile(r'(.*)\.*csv')
    for tipo in SHEET_TIPES:
        print("Uniendo csv de tipo: " + tipo)
        archivos = listdir(carpeta + "/" + tipo)
        csv = list(filter(regex.search, archivos))
        subprocess.run("head -1 " +
                       carpeta + "/" + tipo + "/" + shlex.quote(csv[0]) +
                       " > " + carpeta + "/" + tipo + "/all.csv", shell=True)
        subprocess.run("tail -n +2 -q " +
                       carpeta + "/" + tipo + "/" + "*.csv" +
                       " >> " + carpeta + "/" + tipo + "/all.csv", shell=True)


def main():
    """Obtiene los argumentos y ejecuta el programa
    :returns: TODO

    """
    parser = argparse.ArgumentParser(description='Convierte xlsx a csv')
    parser.add_argument("carpeta",
                        help="Carpeta de los archivos xlsx a convertir",
                        nargs='?', default=".")
    args = parser.parse_args()
    carpeta = args.carpeta
    crear_carpetas(carpeta, SHEET_TIPES)
    archivos = listdir(carpeta)

    regex = re.compile(r'(.*)\.xlsx$')
    xlsx = list(filter(regex.search, archivos))
    output = list(map(lambda s: regex.sub(r'\1.csv', s), xlsx))
    convertir_archivos(xlsx, output, carpeta)
    unir_csv(carpeta, SHEET_TIPES)


if __name__ == "__main__":
    main()
