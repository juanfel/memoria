#!/bin/zsh
docker run \
    --publish=7474:7474 --publish=7687:7687 \
    --volume=$HOME/neo4j/data:/data \
    --volume=$HOME/neo4j/import:/var/lib/neo4j/import \
    --volume=$HOME/neo4j/plugins:/var/lib/neo4j/plugins \
    -e NEO4J_dbms_security_procedures_unrestricted=apoc.\\\*,algo.\\\* \
    -env=DFile_encoding=UTF8 \
    --env=NEO4J_dbms_memory_heap_maxSize=2G \
    --env=NEO4J_dbms_memory_pagecache_size=2G \
    neo4j:latest
